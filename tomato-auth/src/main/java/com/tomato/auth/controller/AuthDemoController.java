package com.tomato.auth.controller;

import com.tomato.api.demo.feign.RemoteDemoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * AuthDemoController
 *
 * @author lizhifu
 * @date 2022/3/3
 */
@RestController
public class AuthDemoController {
    @Resource
    public RemoteDemoService remoteDemoService;
    @GetMapping("/authDemo")
    public String authDemo (){
        return remoteDemoService.demo(UUID.randomUUID().toString());
    }
}
