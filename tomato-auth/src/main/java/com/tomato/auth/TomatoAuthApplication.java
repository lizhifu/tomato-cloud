package com.tomato.auth;

import com.tomato.base.feign.annotation.EnableTomatoFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 认证授权中心
 * @author lizhifu
 */
@SpringBootApplication
@EnableTomatoFeignClients
public class TomatoAuthApplication
{
    public static void main(String[] args) {
        SpringApplication.run(TomatoAuthApplication.class, args);
        System.out.println("认证授权中心启动成功");
    }
}
