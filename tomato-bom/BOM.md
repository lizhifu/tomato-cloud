# BOM
## 什么是BOM
BOM全称是Bill Of Materials，译作材料清单。BOM本身并不是一种特殊的文件格式，而是一个普通的POM文件，只是在这个POM中，我们罗列的是一个工程的所有依赖和其对应的版本。该文件一般被其它工程使用，当其它工程引用BOM中罗列的jar包时，不用显示指定具体的版本，会自动使用BOM对应的jar版本。

所以BOM的好处是用来管理一个工程的所有依赖版本信息。
## 版本冲突时的一些规则
当出现版本冲突时，具体使用哪一个版本的优先顺序是

直接在当前工程中显示指定的版本
parent中配置的父工程使用的版本
在当前工程中通过dependencyManagement引入的BOM清单中的版本，当引入的多个BOM都有对应jar包时，先引入的BOM生效
上述三个地方都没配置，则启用依赖调解dependency mediation

## 依赖调节
当有两个依赖路径，依赖到同一个jar的不同版本时，最短路径的版本生效，比如A -> B -> C -> D 1.4 and A -> E -> D 1.0最终将会使用D的1.0版本