package com.tomato.gateway;

import com.tomato.gateway.loadbalancer.VersionLoadBalancerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClients;

/**
 * 网关
 *
 * @author lizhifu
 * @date 2022/3/2
 */
@SpringBootApplication
// 启用自定义负载均衡算法
@LoadBalancerClient(value = "tomato-demo", configuration = VersionLoadBalancerConfiguration.class)
// 所有服务启用自定义负载均衡算法
//@LoadBalancerClients(defaultConfiguration = VersionLoadBalancerConfiguration.class)
public class TomatoGatewayApplication {

    public static void main(String[] args){
        SpringApplication.run(TomatoGatewayApplication.class, args);
        System.out.println("网关启动成功");
    }
}
