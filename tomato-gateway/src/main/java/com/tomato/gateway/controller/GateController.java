package com.tomato.gateway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * GateController
 *
 * @author lizhifu
 * @date 2022/3/3
 */
@RestController
public class GateController {
    @GetMapping("/gate")
    public void gate(){
        System.out.println(UUID.randomUUID());
    }
}
