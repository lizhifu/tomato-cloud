package com.tomato.gateway.predicate;

import java.util.List;

/**
 * HeaderUserConfig
 *
 * @author lizhifu
 * @date 2022/3/12
 */
public class HeaderUserConfig {
    private List<String> userNo;

    public List<String> getUserNo() {
        return userNo;
    }

    public void setUserNo(List<String> userNo) {
        this.userNo = userNo;
    }
}
