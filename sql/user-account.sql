-- ----------------------------
-- 账号表
-- ----------------------------
drop table if exists `master`.`account_0`;
create table `master`.`account_0`  (
    `id`                bigint(20)      not null auto_increment    comment 'ID',
    `user_id`           bigint(20)      not null                   comment '用户ID',
    `login_account`     varchar(255)    character set utf8mb4 collate utf8mb4_general_ci not null comment '登录账号',

    `status`            char(1)         default '0'                comment '帐号状态（0正常 1停用）',
    `del_flag`          char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
    `create_by`         varchar(64)     default ''                 comment '创建者',
    `create_time`       datetime        not null default current_timestamp comment '创建时间',
    `update_by`         varchar(64)     default ''                 comment '更新者',
    `update_time`       datetime        not null default current_timestamp on update current_timestamp comment '更新时间',
    primary key (`id`) using btree,
    unique key `uk_login_account` (`login_account`) using btree comment '登录账号',
    index `idx_user_id`(`user_id`) using btree comment '用户id'
) engine=innodb auto_increment=100 default charset=utf8mb4 collate=utf8mb4_unicode_ci comment='账号表';

drop table if exists `ds01`.`account_0`;
create table `ds01`.`account_0`  (
     `id`                bigint(20)      not null auto_increment    comment 'ID',
     `user_id`           bigint(20)      not null                   comment '用户ID',
     `login_account`     varchar(255)    character set utf8mb4 collate utf8mb4_general_ci not null comment '登录账号',

     `status`            char(1)         default '0'                comment '帐号状态（0正常 1停用）',
     `del_flag`          char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
     `create_by`         varchar(64)     default ''                 comment '创建者',
     `create_time`       datetime        not null default current_timestamp comment '创建时间',
     `update_by`         varchar(64)     default ''                 comment '更新者',
     `update_time`       datetime        not null default current_timestamp on update current_timestamp comment '更新时间',
     primary key (`id`) using btree,
     unique key `uk_login_account` (`login_account`) using btree comment '登录账号',
     index `idx_user_id`(`user_id`) using btree comment '用户id'
) engine=innodb auto_increment=100 default charset=utf8mb4 collate=utf8mb4_unicode_ci comment='账号表';

drop table if exists `ds02`.`account_0`;
create table `ds02`.`account_0`  (
     `id`                bigint(20)      not null auto_increment    comment 'ID',
     `user_id`           bigint(20)      not null                   comment '用户ID',
     `login_account`     varchar(255)    character set utf8mb4 collate utf8mb4_general_ci not null comment '登录账号',

     `status`            char(1)         default '0'                comment '帐号状态（0正常 1停用）',
     `del_flag`          char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
     `create_by`         varchar(64)     default ''                 comment '创建者',
     `create_time`       datetime        not null default current_timestamp comment '创建时间',
     `update_by`         varchar(64)     default ''                 comment '更新者',
     `update_time`       datetime        not null default current_timestamp on update current_timestamp comment '更新时间',
     primary key (`id`) using btree,
     unique key `uk_login_account` (`login_account`) using btree comment '登录账号',
     index `idx_user_id`(`user_id`) using btree comment '用户id'
) engine=innodb auto_increment=100 default charset=utf8mb4 collate=utf8mb4_unicode_ci comment='账号表';

-- ----------------------------
-- 账号表
-- ----------------------------
drop table if exists `master`.`account_1`;
create table `master`.`account_1`  (
 `id`                bigint(20)      not null auto_increment    comment 'ID',
 `user_id`           bigint(20)      not null                   comment '用户ID',
 `login_account`     varchar(255)    character set utf8mb4 collate utf8mb4_general_ci not null comment '登录账号',

 `status`            char(1)         default '0'                comment '帐号状态（0正常 1停用）',
 `del_flag`          char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
 `create_by`         varchar(64)     default ''                 comment '创建者',
 `create_time`       datetime        not null default current_timestamp comment '创建时间',
 `update_by`         varchar(64)     default ''                 comment '更新者',
 `update_time`       datetime        not null default current_timestamp on update current_timestamp comment '更新时间',
 primary key (`id`) using btree,
 unique key `uk_login_account` (`login_account`) using btree comment '登录账号',
 index `idx_user_id`(`user_id`) using btree comment '用户id'
) engine=innodb auto_increment=100 default charset=utf8mb4 collate=utf8mb4_unicode_ci comment='账号表';

drop table if exists `ds01`.`account_1`;
create table `ds01`.`account_1`  (
`id`                bigint(20)      not null auto_increment    comment 'ID',
`user_id`           bigint(20)      not null                   comment '用户ID',
`login_account`     varchar(255)    character set utf8mb4 collate utf8mb4_general_ci not null comment '登录账号',

`status`            char(1)         default '0'                comment '帐号状态（0正常 1停用）',
`del_flag`          char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
`create_by`         varchar(64)     default ''                 comment '创建者',
`create_time`       datetime        not null default current_timestamp comment '创建时间',
`update_by`         varchar(64)     default ''                 comment '更新者',
`update_time`       datetime        not null default current_timestamp on update current_timestamp comment '更新时间',
primary key (`id`) using btree,
unique key `uk_login_account` (`login_account`) using btree comment '登录账号',
index `idx_user_id`(`user_id`) using btree comment '用户id'
) engine=innodb auto_increment=100 default charset=utf8mb4 collate=utf8mb4_unicode_ci comment='账号表';

drop table if exists `ds02`.`account_1`;
create table `ds02`.`account_1`  (
`id`                bigint(20)      not null auto_increment    comment 'ID',
`user_id`           bigint(20)      not null                   comment '用户ID',
`login_account`     varchar(255)    character set utf8mb4 collate utf8mb4_general_ci not null comment '登录账号',

`status`            char(1)         default '0'                comment '帐号状态（0正常 1停用）',
`del_flag`          char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
`create_by`         varchar(64)     default ''                 comment '创建者',
`create_time`       datetime        not null default current_timestamp comment '创建时间',
`update_by`         varchar(64)     default ''                 comment '更新者',
`update_time`       datetime        not null default current_timestamp on update current_timestamp comment '更新时间',
primary key (`id`) using btree,
unique key `uk_login_account` (`login_account`) using btree comment '登录账号',
index `idx_user_id`(`user_id`) using btree comment '用户id'
) engine=innodb auto_increment=100 default charset=utf8mb4 collate=utf8mb4_unicode_ci comment='账号表';