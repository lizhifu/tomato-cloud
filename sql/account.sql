create database  `ds01`;
create database  `ds02`;
create database  `master`;

drop table if exists `ds01`.`account`;
CREATE TABLE `ds01`.`account` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `balance` decimal not null comment '余额',
    `account_no` varchar(255) NOT NULL COMMENT '账户编号',
    `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_account_account_no` (`account_no`)
) COMMENT='账户信息';

drop table if exists `ds02`.`account`;
CREATE TABLE `ds02`.`account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `balance` decimal not null comment '余额',
  `account_no` varchar(255) NOT NULL COMMENT '账户编号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_account_account_no` (`account_no`)
) COMMENT='账户信息';


drop table if exists `master`.`account`;
CREATE TABLE `master`.`account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `balance` decimal not null comment '余额',
  `account_no` varchar(255) NOT NULL COMMENT '账户编号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_account_account_no` (`account_no`)
) COMMENT='账户信息';