drop table if exists `tomato-id`.`tomato_id`;
create table `tomato-id`.`tomato_id` (
    `id` int(10) not null,
    `biz_type` varchar(128)  not null comment '业务类型',
    `work_id` varchar(128)  not null comment '雪花算法生成的id',
    `max_id` bigint(20) not null default '1' comment '当前最大id',
    `step` int(11) not null comment '号段的布长',
    `sequence_offset` int not null default 0 comment 'sequence偏移量',
    `version` int(20) not null default 0 comment '版本号',
    `update_time` datetime not null default current_timestamp on update current_timestamp,
    `create_time`       datetime        not null default current_timestamp comment '创建时间',
    primary key (`id`)
) engine=innodb auto_increment=100 default charset=utf8mb4 collate=utf8mb4_unicode_ci comment='分布式ID';

insert into `tomato-id`.tomato_id (id, biz_type,work_id, step,sequence_offset)
 values (1, 'tomato-modules-user','1,2,3', 1000,100);