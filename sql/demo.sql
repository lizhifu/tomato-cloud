create database  `demo`;
drop table if exists `demo`.`demo`;
CREATE TABLE `demo`.`demo` (
`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
`a1` varchar(255) NOT NULL COMMENT 'a1',
`a2` varchar(255) NOT NULL COMMENT 'a2',
`a3` varchar(255) NOT NULL COMMENT 'a3',
`a4` varchar(255) NOT NULL COMMENT 'a4',
PRIMARY KEY (`id`),
index demo_idnex(a1,a2,a3)
) COMMENT='demo';


drop table if exists `demo`.`demo1`;
CREATE TABLE `demo`.`demo1` (
`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
`a1` varchar(255) NOT NULL COMMENT 'a1',
`a2` varchar(255) NOT NULL COMMENT 'a2',
`a3` varchar(255) NOT NULL COMMENT 'a3',
`a4` varchar(255) NOT NULL COMMENT 'a4',
PRIMARY KEY (`id`),
index demo1_idnex1(a1),
index demo1_idnex2(a2),
index demo1_idnex3(a3)
) COMMENT='demo1';