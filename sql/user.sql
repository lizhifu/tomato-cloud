-- ----------------------------
-- 账号表
-- ----------------------------
drop table if exists `account`;
create table `account`  (
    `id`                bigint(20)      not null auto_increment    comment 'ID',
    `user_id`           bigint(20)      not null                   comment '用户ID',
    `login_account`     varchar(255)    character set utf8mb4 collate utf8mb4_general_ci not null comment '登录账号',
    `account_type`      tinyint(1)      not null                   comment '账号类别',

    `status`            char(1)         default '0'                comment '帐号状态（0正常 1停用）',
    `del_flag`          char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
    `create_by`         varchar(64)     default ''                 comment '创建者',
    `create_time`       datetime        not null default current_timestamp comment '创建时间',
    `update_by`         varchar(64)     default ''                 comment '更新者',
    `update_time`       datetime        not null default current_timestamp on update current_timestamp comment '更新时间',
    primary key (`id`) using btree,
    unique key `uk_login_account` (`login_account`) using btree comment '登录账号',
    index `idx_user_id`(`user_id`) using btree comment '用户id'
) engine=innodb auto_increment=100 default charset=utf8mb4 collate=utf8mb4_unicode_ci comment='账号表';

-- ----------------------------
-- 菜单权限表
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '权限ID',
`parent_id` bigint(20) NULL DEFAULT NULL COMMENT '所属父级权限ID',
`code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限唯一CODE代码',
`name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限名称',
`intro` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限介绍',
`category` tinyint(1) NULL DEFAULT NULL COMMENT '权限类别',
`uri` bigint(20) NULL DEFAULT NULL COMMENT 'URL规则',
`created` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
`creator` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
`edited` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
`editor` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
`deleted` tinyint(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '逻辑删除:0=未删除,1=已删除',
PRIMARY KEY (`id`) USING BTREE,
INDEX `parent_id`(`parent_id`) USING BTREE COMMENT '父级权限ID',
INDEX `code`(`code`) USING BTREE COMMENT '权限CODE代码'
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限' ROW_FORMAT = Dynamic;

-- ----------------------------
-- 角色表
-- ----------------------------
CREATE TABLE `role`  (
`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
`parent_id` bigint(20) NULL DEFAULT NULL COMMENT '所属父级角色ID',
`code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色唯一CODE代码',
`name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
`intro` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色介绍',
`created` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
`creator` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
`edited` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
`editor` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
`deleted` tinyint(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '逻辑删除:0=未删除,1=已删除',
PRIMARY KEY (`id`) USING BTREE,
INDEX `parent_id`(`parent_id`) USING BTREE COMMENT '父级权限ID',
INDEX `code`(`code`) USING BTREE COMMENT '权限CODE代码'
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- 用户—角色表
-- ----------------------------
CREATE TABLE `user_role`  (
`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
`user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
`role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
`created` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
`creator` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
`edited` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
`editor` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
`deleted` tinyint(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '逻辑删除:0=未删除,1=已删除',
PRIMARY KEY (`id`) USING BTREE,
INDEX `member_id`(`user_id`) USING BTREE COMMENT '用户ID',
INDEX `role_id`(`role_id`) USING BTREE COMMENT '角色ID',
CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- 角色—权限表
-- ----------------------------
CREATE TABLE `role_permission`  (
`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
`role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
`permission_id` bigint(20) NULL DEFAULT NULL COMMENT '权限ID',
`created` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
`creator` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
`edited` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
`editor` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
`deleted` tinyint(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '逻辑删除:0=未删除,1=已删除',
PRIMARY KEY (`id`) USING BTREE,
INDEX `role_id`(`role_id`) USING BTREE COMMENT '角色ID',
INDEX `permission_id`(`permission_id`) USING BTREE COMMENT '权限ID',
CONSTRAINT `role_permission_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
CONSTRAINT `role_permission_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色权限' ROW_FORMAT = Dynamic;

-- ----------------------------
-- 用户组表
-- ----------------------------
CREATE TABLE `user_group`  (
`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
`parent_id` bigint(20) NULL DEFAULT NULL COMMENT '所属父级用户组ID',
`name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户组名称',
`code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户组CODE唯一代码',
`intro` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户组介绍',
`created` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
`creator` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
`edited` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
`editor` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
`deleted` tinyint(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '逻辑删除:0=未删除,1=已删除',
PRIMARY KEY (`id`) USING BTREE,
INDEX `parent_id`(`parent_id`) USING BTREE COMMENT '父级用户组ID',
INDEX `code`(`code`) USING BTREE COMMENT '用户组CODE代码'
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户组' ROW_FORMAT = Dynamic;
-- ----------------------------
-- 用户组—用户表
-- ----------------------------
CREATE TABLE `user_group_user`  (
`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID说',
`user_group_id` bigint(20) NULL DEFAULT NULL COMMENT '用户组ID',
`user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
`created` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
`creator` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
`edited` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
`editor` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
`deleted` tinyint(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '逻辑删除:0=未删除,1=已删除',
PRIMARY KEY (`id`) USING BTREE,
INDEX `member_group_id`(`user_group_id`) USING BTREE COMMENT '用户组ID',
INDEX `member_id`(`user_id`) USING BTREE COMMENT '用户ID',
CONSTRAINT `user_group_user_ibfk_1` FOREIGN KEY (`user_group_id`) REFERENCES `user_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
CONSTRAINT `user_group_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户组成员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- 用户组—角色表
-- ----------------------------
CREATE TABLE `user_group_role`  (
`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
`user_group_id` bigint(20) NULL DEFAULT NULL COMMENT '用户组ID',
`role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
`created` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
`creator` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
`edited` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
`editor` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
`deleted` tinyint(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '逻辑删除:0=未删除,1=已删除',
PRIMARY KEY (`id`) USING BTREE,
INDEX `member_group_id`(`user_group_id`) USING BTREE COMMENT '用户组ID',
INDEX `role_id`(`role_id`) USING BTREE COMMENT '角色ID',
CONSTRAINT `user_group_role_ibfk_1` FOREIGN KEY (`user_group_id`) REFERENCES `user_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
CONSTRAINT `user_group_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户组角色' ROW_FORMAT = Dynamic;