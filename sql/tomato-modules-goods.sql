CREATE DATABASE `tomato-goods`;
DROP TABLE IF EXISTS `tomato-goods`.`goods`;
CREATE TABLE `goods` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
    `goods_id` bigint(20) NOT NULL COMMENT '商品ID',
    `stock_count` int(11) DEFAULT NULL COMMENT '库存',
    `stock_left_count` int(11) DEFAULT NULL COMMENT '库存剩余',
    `goods_state` tinyint(2) DEFAULT NULL COMMENT '商品状态：0关闭、1打开',
    `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `unique_goods_id` (`goods_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='商品信息';