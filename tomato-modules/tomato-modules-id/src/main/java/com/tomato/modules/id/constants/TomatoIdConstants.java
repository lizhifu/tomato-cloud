package com.tomato.modules.id.constants;

/**
 * 分布式 ID 常量
 *
 * @author lizhifu
 * @date 2022/3/18
 */
public class TomatoIdConstants {
    /**
     * 预加载下个号段的百分比
     */
    public static final int LOADING_PERCENT = 20;
    /**
     * 重试次数
     */
    public static final int RETRY = 3;
}
