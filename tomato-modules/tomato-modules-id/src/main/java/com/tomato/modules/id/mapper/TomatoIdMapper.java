package com.tomato.modules.id.mapper;

import com.tomato.api.id.dataobject.TomatoIdDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 分布式 ID
 *
 * @author lizhifu
 * @date 2022/3/17
 */
@Mapper
public interface TomatoIdMapper {
    /**
     * 更新 max_id
     * @param bizType 业务类型
     * @param version 版本号
     */
    @Update("UPDATE tomato_id SET max_id = max_id + step ,version = version + 1 WHERE version = #{version} and biz_type = #{bizType,jdbcType=VARCHAR}")
    int updateMaxId(@Param("bizType") String bizType,@Param("version") Integer version);

    /**
     * 根据业务类型查询
     * @param bizType 业务类型
     * @return
     */
    @Select("select * from tomato_id where biz_type = #{bizType,jdbcType=VARCHAR}")
    TomatoIdDO selectByBizType(@Param("bizType") String bizType);

    @Select("select * from tomato_id")
    List<TomatoIdDO> selectAll();
}
