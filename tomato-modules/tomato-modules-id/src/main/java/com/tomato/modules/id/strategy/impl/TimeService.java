package com.tomato.modules.id.strategy.impl;

/**
 * 获取时间戳的服务
 *
 * @author lizhifu
 * @date 2022/4/6
 */
public class TimeService {
    /**
     * 获取当前时间戳
     *
     * @return 当前时间戳
     */
    public long getCurrentTime() {
        return System.currentTimeMillis();
    }
}
