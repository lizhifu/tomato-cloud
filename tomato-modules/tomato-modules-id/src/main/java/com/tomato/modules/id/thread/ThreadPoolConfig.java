package com.tomato.modules.id.thread;

import com.tomato.dynamic.threadpool.core.manage.GlobalThreadPoolManage;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 线程池配置
 *
 * @author lizhifu
 * @date 2022/4/7
 */
public class ThreadPoolConfig {
    public static ThreadPoolExecutor getCachedIdGeneratorStrategy() {
        return GlobalThreadPoolManage.getExecutorService("tomato-id-generator");
    }
}
