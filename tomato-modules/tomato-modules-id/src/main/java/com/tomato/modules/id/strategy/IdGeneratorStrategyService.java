package com.tomato.modules.id.strategy;

/**
 * ID 生成
 *
 * @author lizhifu
 * @date 2022/3/17
 */
@FunctionalInterface
public interface IdGeneratorStrategyService {
    /**
     * 获取 ID
     * @return
     */
    Long nextId();
}
