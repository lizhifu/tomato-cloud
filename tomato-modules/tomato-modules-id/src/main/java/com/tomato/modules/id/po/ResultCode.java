package com.tomato.modules.id.po;

/**
 * @author lizhifu
 */
public class ResultCode {
    /**
     * 正常可用
     */
    public static final String NORMAL = "NORMAL";
    /**
     * 需要去加载nextId
     */
    public static final String LOADING = "LOADING";
    /**
     * 超过maxId 不可用
     */
    public static final String OVER = "OVER";
}
