package com.tomato.modules.id.strategy;

/**
 * 不同的 ID 生成策略
 *
 * @author lizhifu
 * @date 2022/3/19
 */
public class IdGenerator {
    private IdGeneratorStrategyService idGeneratorStrategyService;
    public IdGenerator (IdGeneratorStrategyService idGeneratorStrategyService){
        this.idGeneratorStrategyService = idGeneratorStrategyService;
    }

    public Long nextId(){
        return idGeneratorStrategyService.nextId();
    }
}
