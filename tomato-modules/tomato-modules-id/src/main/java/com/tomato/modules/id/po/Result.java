package com.tomato.modules.id.po;

/**
 * @author du_imba
 */
public class Result {
    private String code;
    private long id;

    public Result(String code, long id) {
        this.code = code;
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
