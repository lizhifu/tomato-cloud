package com.tomato.modules.id.service;

import com.tomato.modules.id.po.SegmentId;

/**
 * 分布式 ID
 *
 * @author lizhifu
 * @date 2022/3/17
 */
public interface SegmentIdService {
    /**
     * 更新 maxId
     * @param bizType
     * @return
     */
    SegmentId updateMaxId(String bizType);
}
