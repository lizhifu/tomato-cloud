package com.tomato.modules.id;

import com.tomato.base.feign.annotation.EnableTomatoFeignClients;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 分布式 id
 *
 * @author lizhifu
 * @date 2022/3/2
 */
@SpringBootApplication
@EnableTomatoFeignClients
@MapperScan("com.tomato.modules.id.mapper")//使用MapperScan批量扫描所有的Mapper接口
public class TomatoIdApplication {
    public static void main(String[] args){
        SpringApplication.run(TomatoIdApplication.class, args);
        System.out.println("分布式 id 服务启动成功");
    }
}
