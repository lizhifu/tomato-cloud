package com.tomato.modules.id;

import com.tomato.modules.id.runner.IdGeneratorRunner;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * IdGeneratorService
 *
 * @author lizhifu
 * @date 2022/3/18
 */
@SpringBootTest
public class IdGeneratorStrategyServiceTest {
    @Resource
    private IdGeneratorRunner idGeneratorRunner;
    @Test
    public void test(){
        for (int i = 0; i < 20; i++) {
            Long nextId = idGeneratorRunner.getGenerators("test1").nextId();
            System.out.println("test1第"+i+"获取："+nextId);
        }
        for (int i = 0; i < 20; i++) {
            Long nextId = idGeneratorRunner.getGenerators("test2").nextId();
            System.out.println("test2:第"+i+"获取："+nextId);
        }
    }
}
