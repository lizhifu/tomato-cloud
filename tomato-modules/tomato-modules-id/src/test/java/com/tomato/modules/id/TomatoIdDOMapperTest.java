package com.tomato.modules.id;

import com.tomato.api.id.dataobject.TomatoIdDO;
import com.tomato.modules.id.mapper.TomatoIdMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * TomatoIdMapper
 *
 * @author lizhifu
 * @date 2022/3/18
 */
@SpringBootTest
public class TomatoIdDOMapperTest {
    @Resource
    TomatoIdMapper tomatoIdMapper;
    @Test
    public void test(){
        TomatoIdDO test = tomatoIdMapper.selectByBizType("test");
        System.out.println(test);
    }
}
