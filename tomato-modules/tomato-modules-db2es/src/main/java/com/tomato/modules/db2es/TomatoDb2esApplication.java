package com.tomato.modules.db2es;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * db2es
 *
 * @author lizhifu
 * @date 2022/5/5
 */
@SpringBootApplication
@MapperScan("com.tomato.modules.db2es.mapper")//使用MapperScan批量扫描所有的Mapper接口
public class TomatoDb2esApplication {
    public static void main(String[] args){
        SpringApplication.run(TomatoDb2esApplication.class, args);
        System.out.println("用户服务 服务启动成功");
    }
}
