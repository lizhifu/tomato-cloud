package com.tomato.modules.user;

import com.tomato.base.feign.annotation.EnableTomatoFeignClients;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 用户服务
 *
 * @author lizhifu
 * @date 2022/3/31
 */
@SpringBootApplication
@EnableTomatoFeignClients
@MapperScan("com.tomato.modules.user.mapper")//使用MapperScan批量扫描所有的Mapper接口
public class TomatoUserApplication {
    public static void main(String[] args){
        SpringApplication.run(TomatoUserApplication.class, args);
        System.out.println("用户服务 服务启动成功");
    }
}
