package com.tomato.modules.user.service.impl;

import com.tomato.api.user.exception.UserException;
import com.tomato.api.user.dataobject.AccountDO;
import com.tomato.modules.user.mapper.AccountMapper;
import com.tomato.modules.user.service.AccountService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 账户服务实现类
 *
 * @author lizhifu
 * @date 2022/4/4
 */
@Service
public class AccountServiceImpl implements AccountService {
    @Resource
    private AccountMapper accountMapper;
    @Override
    public void checkAccount(String loginAccount) {
        AccountDO accountPO = accountMapper.selectAccountByLoginAccount(loginAccount);
        if (accountPO == null) {
            return;
        }
        throw new UserException("账户:"+loginAccount+"已经存在");
    }
}
