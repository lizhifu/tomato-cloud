package com.tomato.modules.user.service.impl;

import com.tomato.api.user.dataobject.AccountDO;
import com.tomato.api.user.dataobject.UserDO;
import com.tomato.modules.user.mapper.AccountMapper;
import com.tomato.modules.user.mapper.UserMapper;
import com.tomato.modules.user.service.AccountService;
import com.tomato.modules.user.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 用户服务实现类
 *
 * @author lizhifu
 * @date 2022/4/2
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private AccountService accountService;
    @Resource
    private UserMapper userMapper;
    @Resource
    private AccountMapper accountMapper;
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createUser(UserDO userPO, AccountDO accountPO) {
        userMapper.insertUser(userPO);
        accountService.checkAccount(accountPO.getLoginAccount());
        accountMapper.insertAccount(accountPO);
    }
}
