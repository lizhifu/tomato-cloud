package com.tomato.modules.user.mapper;

import com.tomato.api.user.dataobject.AccountDO;
import com.tomato.base.dynamic.db.annotation.DynamicDataSourceAnnotation;
import com.tomato.base.dynamic.db.annotation.DynamicTableAnnotation;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * account mapper
 *
 * @author lizhifu
 * @date 2022/4/2
 */
@Mapper
public interface AccountMapper {
    /**
     * 创建账户信息
     * @param accountPO
     */
    @DynamicDataSourceAnnotation(key = "userId")
    @DynamicTableAnnotation(key = "userId",table = "account")
    void insertAccount(AccountDO accountPO);

    /**
     * 根据用户id查询账户信息
     * @param userId
     * @return
     */
    @DynamicDataSourceAnnotation(key = "userId")
    @DynamicTableAnnotation(key = "userId",table = "account")
    List<AccountDO> selectAccountByUserId(Long userId);

    /**
     * 根据登录账号查询账户信息
     * @param loginAccount
     * @return
     */
    @DynamicDataSourceAnnotation(key = "userId")
    @DynamicTableAnnotation(key = "userId",table = "account")
    AccountDO selectAccountByLoginAccount(String loginAccount);
}
