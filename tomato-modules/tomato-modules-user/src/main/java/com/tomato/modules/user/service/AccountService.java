package com.tomato.modules.user.service;

/**
 * 账号服务
 *
 * @author lizhifu
 * @date 2022/4/4
 */
public interface AccountService {
    /**
     * 校验账户名是否存在
     * @param loginAccount
     */
    void checkAccount(String loginAccount);
}
