package com.tomato.modules.user.service;

import com.tomato.api.user.dataobject.AccountDO;
import com.tomato.api.user.dataobject.UserDO;
/**
 * 用户服务
 *
 * @author lizhifu
 * @date 2022/4/2
 */
public interface UserService {
    /**
     * 创建用户
     * @param userPO 用户信息
     * @param accountPO 账号信息
     */
    void createUser(UserDO userPO, AccountDO accountPO);
}
