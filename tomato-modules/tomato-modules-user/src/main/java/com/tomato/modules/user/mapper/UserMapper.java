package com.tomato.modules.user.mapper;

import com.tomato.api.user.dataobject.UserDO;
import com.tomato.base.dynamic.db.annotation.DynamicDataSourceAnnotation;
import com.tomato.base.dynamic.db.annotation.DynamicTableAnnotation;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户表
 *
 * @author lizhifu
 * @date 2022/3/31
 */
@Mapper
public interface UserMapper {
    /**
     * 新增用户信息
     *
     * @param userPO 用户信息
     * @return 结果
     */
    @DynamicDataSourceAnnotation(key = "userId")
    @DynamicTableAnnotation(key = "userId", table = "user")
    public int insertUser(UserDO userPO);
}
