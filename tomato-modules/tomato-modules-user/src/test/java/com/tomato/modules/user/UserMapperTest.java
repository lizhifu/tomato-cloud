package com.tomato.modules.user;

import com.tomato.api.user.dataobject.UserDO;
import com.tomato.modules.user.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * UserMapper
 *
 * @author lizhifu
 * @date 2022/4/1
 */
@SpringBootTest
public class UserMapperTest {
    @Resource
    private UserMapper userMapper;
    @Test
    public void test(){
        UserDO userPO = new UserDO();
        userPO.setUserId(1002L);
        userPO.setPassword("123");
        userMapper.insertUser(userPO);
    }
}
