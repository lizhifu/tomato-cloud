package com.tomato.modules.account;

import com.tomato.modules.account.service.AccountRedisService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * AccountRedisService
 *
 * @author lizhifu
 * @date 2022/3/8
 */
@SpringBootTest
public class AccountRedisServiceTest {
    @Resource
    AccountRedisService accountRedisService;
    @Test
    public void test(){
        accountRedisService.accountBalance("123456",new BigDecimal(100));
    }
}
