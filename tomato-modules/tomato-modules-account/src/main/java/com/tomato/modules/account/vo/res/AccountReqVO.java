package com.tomato.modules.account.vo.res;

import lombok.Data;

/**
 * AccountReqVO
 *
 * @author lizhifu
 * @date 2022/3/13
 */
@Data
public class AccountReqVO {
    private String accountNo;
}
