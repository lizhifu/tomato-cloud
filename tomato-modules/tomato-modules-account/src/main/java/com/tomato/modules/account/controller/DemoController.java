package com.tomato.modules.account.controller;

import com.tomato.base.controller.annotation.ApiVersion;
import com.tomato.base.core.dto.Response;
import com.tomato.modules.account.exception.AccountCustomException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * demo
 * http://localhost:8080/demo/demo
 *
 * @author lizhifu
 * @date 2021/9/15
 */
@RestController
@RequestMapping("/demo")
@Slf4j
public class DemoController {
    @RequestMapping("/demo")
    public Response demo(){
        return Response.buildSuccess();
    }

    @ApiVersion
    @GetMapping(value = "{version}/")
    public String index(int i){
        if(i >= 10){
            throw new AccountCustomException("A400","业务错误");
        }
        return "index";
    }

    @ApiVersion(value = 2)
    @GetMapping(value = "{version}/")
    public String index2(int i){
        if(i >= 10){
            throw new AccountCustomException("A400","业务错误");
        }
        return "index2";
    }
}
