package com.tomato.modules.account.controller;

import com.tomato.base.controller.controller.BaseController;
import com.tomato.base.core.dto.Response;
import com.tomato.base.core.dto.SingleResponse;
import com.tomato.modules.account.entity.AccountEntity;
import com.tomato.modules.account.mapper.AccountMapper;
import com.tomato.modules.account.vo.res.AccountReqVO;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 *  Account
 *
 * @author lizhifu
 * @date 2022/3/13
 */
@RestController
public class AccountController extends BaseController {
    @Resource
    private AccountMapper accountMapper;

    @GetMapping("/account/balance/{accountNo}")
    public Response balance(@PathVariable("accountNo") String accountNo){
        BigDecimal balance = accountMapper.balance(accountNo);
        return SingleResponse.of(balance);
    }

    @PostMapping ("/account/insert")
    public Response insert(@RequestBody AccountReqVO accountReqVO){
        AccountEntity accountEntity = new AccountEntity();
        accountEntity.setAccountNo(accountReqVO.getAccountNo());
        accountEntity.setBalance(new BigDecimal(0));

        Long id = accountMapper.insert(accountEntity);
        return SingleResponse.of(id);
    }
}
