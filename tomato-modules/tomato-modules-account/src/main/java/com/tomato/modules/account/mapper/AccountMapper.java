package com.tomato.modules.account.mapper;

import com.tomato.base.dynamic.db.annotation.DynamicDataSourceAnnotation;
import com.tomato.modules.account.entity.AccountEntity;
import org.apache.ibatis.annotations.*;

import java.math.BigDecimal;

/**
 * 账户
 *
 * @author lizhifu
 * @date 2022/3/8
 */
@Mapper
public interface AccountMapper {
    /**
     * 获取账户余额
     * @param accountNo 账户编号
     * @return
     */
    @Select("select balance from account where account_no = #{accountNo}")
    BigDecimal balance(String accountNo);

    /**
     * 创建账户
     * @param account
     * @return
     */
    @Insert("insert into account(account_no,balance) values (#{account.accountNo},#{account.balance})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @DynamicDataSourceAnnotation(key = "",specialDataSource = "ds01")
    Long insert(@Param("account") AccountEntity account);
}
