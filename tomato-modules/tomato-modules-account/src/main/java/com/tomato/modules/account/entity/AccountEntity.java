package com.tomato.modules.account.entity;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Account
 *
 * @author lizhifu
 * @date 2022/3/13
 */
@Getter
@Setter
public class AccountEntity {
    private Long id;
    private BigDecimal balance;
    private String accountNo;
    private LocalDateTime createTime;
}
