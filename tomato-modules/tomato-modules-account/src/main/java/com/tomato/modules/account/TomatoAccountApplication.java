package com.tomato.modules.account;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 账户
 *
 * @author lizhifu
 * @date 2022/3/2
 */
@SpringBootApplication
@MapperScan("com.tomato.modules.account.mapper")//使用MapperScan批量扫描所有的Mapper接口
public class TomatoAccountApplication {
    public static void main(String[] args){
        SpringApplication.run(TomatoAccountApplication.class, args);
        System.out.println("账户 服务启动成功");
    }
}
