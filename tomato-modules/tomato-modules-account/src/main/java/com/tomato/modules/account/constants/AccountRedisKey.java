package com.tomato.modules.account.constants;

/**
 * 账户使用 redis key
 *
 * @author lizhifu
 * @date 2022/3/8
 */
public class AccountRedisKey {
    /**
     *  账户余额 Key
     */
    private static final String ACCOUNT_NO = "account_no_";

    /**
     * 账户编号 key
     * @param accountNo
     * @return
     */
    public static String ACCOUNT_NO(String accountNo) {
        return ACCOUNT_NO + accountNo;
    }
}
