package com.tomato.modules.account.exception;

import com.tomato.base.core.exception.CustomException;

/**
 * AccountCustomException
 *
 * @author lizhifu
 * @date 2022/3/13
 */
public class AccountCustomException extends CustomException {
    public AccountCustomException(String errMessage) {
        super(errMessage);
    }

    public AccountCustomException(String errCode, String errMessage) {
        super(errCode, errMessage);
    }

    public AccountCustomException(String errMessage, Throwable e) {
        super(errMessage, e);
    }

    public AccountCustomException(String errCode, String errMessage, Throwable e) {
        super(errCode, errMessage, e);
    }
}
