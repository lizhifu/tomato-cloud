package com.tomato.modules.account.service;

import java.math.BigDecimal;

/**
 * 账户 redis 操作
 *
 * @author lizhifu
 * @date 2022/3/8
 */
public interface AccountRedisService {
    /**
     * 账号余额操作
     * @param accountNo 账号编号
     * @param amount 金额
     */
    void accountBalance(String accountNo, BigDecimal amount);
}
