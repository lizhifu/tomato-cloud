package com.tomato.modules.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * demo
 *
 * @author lizhifu
 * @date 2022/3/2
 */
@RestController
public class DemoController {
    @GetMapping("/demo/{name}")
    public String demo(@PathVariable("name") String name){
        System.out.println("name:" + name);
        return name;
    }
    @GetMapping("/demo/demo/{name}")
    public String demo2(@PathVariable("name") String name){
        System.out.println("name:" + name);
        return name;
    }
}
