package com.tomato.modules.demo;

import com.tomato.base.feign.annotation.EnableTomatoFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * demo
 *
 * @author lizhifu
 * @date 2022/3/2
 */
@SpringBootApplication
@EnableTomatoFeignClients
public class TomatoDemoApplication {
    public static void main(String[] args){
        SpringApplication.run(TomatoDemoApplication.class, args);
        System.out.println("demo 服务启动成功");
    }
}
