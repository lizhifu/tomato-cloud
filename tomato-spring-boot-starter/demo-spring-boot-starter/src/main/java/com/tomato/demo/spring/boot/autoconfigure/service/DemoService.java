package com.tomato.demo.spring.boot.autoconfigure.service;

/**
 * DemoService
 *
 * @author lizhifu
 * @date 2022/3/14
 */
public interface DemoService {
    public void demo();
}
