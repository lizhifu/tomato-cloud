package com.tomato.demo.spring.boot.autoconfigure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * DemoProperties
 *
 * @author lizhifu
 * @date 2022/3/13
 */
@ConfigurationProperties("demo")
public class DemoProperties {

}
