package com.tomato.demo.spring.boot.autoconfigure.service.impl;

import com.tomato.demo.spring.boot.autoconfigure.service.DemoService;

/**
 * DemoService
 *
 * @author lizhifu
 * @date 2022/3/14
 */
public class DemoServiceImpl implements DemoService {
    @Override
    public void demo() {
        System.out.println("demo");
    }
}
