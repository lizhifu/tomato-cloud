package com.tomato.demo.spring.boot.autoconfigure;

import com.tomato.demo.spring.boot.autoconfigure.properties.DemoProperties;
import com.tomato.demo.spring.boot.autoconfigure.service.DemoService;
import com.tomato.demo.spring.boot.autoconfigure.service.impl.DemoServiceImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * DemoAutoConfiguration
 *
 * @author lizhifu
 * @date 2022/3/13
 */
// 表示该类是一个配置类
@Configuration
// 该注解的作用是为 DemoProperties 开启属性配置功能，并将这个类以组件的形式注入到容器中
@EnableConfigurationProperties(DemoProperties.class)
// 当指定的配置项等于你想要的时候，配置类生效
// @ConditionalOnProperty(prefix = "xxx", name= "x", matchIfMissing = true)
public class DemoAutoConfiguration {

    // 将方法的返回值以 Bean 对象的形式添加到容器中
    @Bean
    // 当容器中没有 DemoServiceImpl 类时，该方法才生效
    @ConditionalOnMissingBean(DemoService.class)
    public DemoService demoService() {
        return new DemoServiceImpl();
    }
}
