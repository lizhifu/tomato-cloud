package com.tomato.test;

import com.tomato.dynamic.threadpool.core.annotation.EnableDynamicThread;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 测试
 *
 * @author lizhifu
 * @date 2022/3/2
 */
//@SpringBootApplication
//@EnableDynamicThread
public class TomatoTestApplication {

    public static void main(String[] args){
        SpringApplication.run(TomatoTestApplication.class, args);
        System.out.println("测试启动成功");
    }
}
