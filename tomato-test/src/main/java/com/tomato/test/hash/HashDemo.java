package com.tomato.test.hash;

/**
 * hash
 *
 * @author lizhifu
 * @date 2022/5/6
 */
public class HashDemo {
    public static void main(String[] args) {
//        System.out.println(Long.toBinaryString(Long.MAX_VALUE));
//        System.out.println(0x7777777777777777L);
        Integer x = new Integer(46);
//        System.out.println(x);
//        System.out.println(x.hashCode());
//        System.out.println(spread(x.hashCode()));

        int hash = spread(x.hashCode());
        System.out.println("hash: " + hash);
        System.out.println("hash: " + (hash & 3));
        int start = (hash & 3) << 2;
        System.out.println(start);


    }
    public static int spread(int x) {
        x = ((x >>> 16) ^ x) * 0x45d9f3b;
        x = ((x >>> 16) ^ x) * 0x45d9f3b;
        return (x >>> 16) ^ x;
    }
}
