package com.tomato.test.thread;

import com.tomato.dynamic.threadpool.core.thread.DynamicThreadPoolExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * ThreadController
 *
 * @author lizhifu
 * @date 2022/4/8
 */
@RestController
@Slf4j
public class ThreadController {
    @Resource(name = "test-thread-factory")
    private DynamicThreadPoolExecutor threadPoolExecutor;
    @GetMapping("/thread")
    public void run(){
        threadPoolExecutor.execute(()->{
            log.info("ThreadController2:" + Thread.currentThread().getName());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }
}
