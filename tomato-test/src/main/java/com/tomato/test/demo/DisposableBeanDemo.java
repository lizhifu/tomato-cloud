package com.tomato.test.demo;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;

/**
 * DisposableBeanDemo
 *
 * @author lizhifu
 * @date 2022/4/8
 */
@Component
public class DisposableBeanDemo implements DisposableBean {
    @Override
    public void destroy() throws Exception {
        System.out.println("DisposableBeanDemo destroy");
    }
}
