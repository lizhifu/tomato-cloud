package com.tomato.test.demo;

import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Demo01
 *
 * @author lizhifu
 * @date 2022/3/8
 */
public class Demo02 {
    public static void main(String[] args) throws IOException {
        ClassPathResource classPathResource = new ClassPathResource("file/test.txt");
        File file = classPathResource.getFile();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        StringBuilder content = new StringBuilder();
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            content.append(line);
        }
        System.out.println(content.toString());
    }
}
