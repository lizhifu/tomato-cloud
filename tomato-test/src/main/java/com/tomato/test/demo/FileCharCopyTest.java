package com.tomato.test.demo;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;

public class FileCharCopyTest {

    public static void main(String[] args) {
        // 方式一：使用原始异常处理机制实现
        FileReader fr = null;
        FileWriter fw = null;

        try {
            // 1.创建FileReader类型的对象与d:/a.txt文件关联
            // fr = new FileReader("d:/a.txt");
            //fr = new FileReader("d:/03 IO流的框架图.png");
            // 2.创建FileWriter类型的对象与d:/b.txt文件关联
            // fw = new FileWriter("d:/b.txt");
            // fw = new FileWriter("d:/IO流的框架图.png");   拷贝图片文件失败！！！
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            File fileA = new File(Objects.requireNonNull(classLoader.getResource("file/a.txt")).getFile());
            File fileB = new File(Objects.requireNonNull(classLoader.getResource("file/b.txt")).getFile());
            fr = new FileReader(fileA);
            fw = new FileWriter(fileB);
            // 3.不断地从输入流中读取数据内容并写入到输出流中
            System.out.println("正在玩命地拷贝...");
            int res = 0;
            while ((res = fr.read()) != -1) {
                System.out.println("读取到的字符是：" + (char)res);
                fw.write(res);
            }
            System.out.println("拷贝文件成功！");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 4.关闭流对象并释放有关的资源
            if (null != fw) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != fr) {
                try {
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        // 方式二：使用Java7开始增加的try with resource新特性来处理异常
        // try(FileReader fr = new FileReader("d:/a.txt");
        // FileWriter fw = new FileWriter("d:/b.txt")) {
        /*ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        File fileA = new File(Objects.requireNonNull(classLoader.getResource("file/a.txt")).getFile());
        File fileB = new File(Objects.requireNonNull(classLoader.getResource("file/b.txt")).getFile());
        try(FileReader fr = new FileReader(fileA);
            FileWriter fw = new FileWriter(fileB)) {
                System.out.println("正在玩命地拷贝...");
                int res = 0;
                while ((res = fr.read()) != -1) {
                    fw.write(res);
                }
                System.out.println("拷贝文件成功！");
            } catch (IOException e) {
                e.printStackTrace();
            }*/
    }
}
