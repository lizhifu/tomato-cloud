package com.tomato.test.demo;

/**
 * Demo04
 *
 * @author lizhifu
 * @date 2022/4/14
 */
public class Demo04 {
    public static void main(String[] args) {
        // 分库分表字段值
        int[] arr = {11111,31826481,352,4595};
        // 一个库中表数量
        int tableCount = 8;
        // 库数量
        int dbCount = 8;
        for (int j = 0; j < arr.length; j++) {
            System.out.println("==========================");
            System.out.println(arr[j] / dbCount);
            System.out.println(arr[j] % dbCount);
            System.out.println(arr[j] / dbCount % tableCount);
            System.out.println(arr[j] % (tableCount * dbCount) / dbCount);
        }
    }
}
