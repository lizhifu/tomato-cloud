package com.tomato.test.demo;

/**
 * MathDemo
 *
 * @author lizhifu
 * @date 2022/4/14
 */
public class MathDemo {
    public static void main(String[] args) {
        int a = 10111;
        for (int i = 1; i <= 32; i++) {
            System.out.println("i="+i+":" +a % i);
        }

    }
}
