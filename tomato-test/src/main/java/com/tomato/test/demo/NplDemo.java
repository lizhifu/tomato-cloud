package com.tomato.test.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * NplDemo
 *
 * @author lizhifu
 * @date 2022/5/5
 */
public class NplDemo {
    public static void main(String[] args) {
        List list = new ArrayList(10);
        list.add(1);
        int a = Optional.ofNullable(list).map(List::size).orElse(0);
        System.out.println(a);
    }
}
