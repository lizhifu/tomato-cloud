package com.tomato.test.demo;

import java.io.*;
import java.util.Objects;

public class FileCharCopyDemo {

    public static void main(String[] args) throws IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        File fileB = new File(classLoader.getResource("file/b.txt").getFile());

        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileB));
        StringBuilder content = new StringBuilder();
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            content.append(line);
        }
        System.out.println("最新文件内容："+content);

        BufferedWriter out = new BufferedWriter(new FileWriter(fileB.getAbsoluteFile()));
        out.write("菜鸟教程");
        out.close();
        System.out.println("文件创建成功！");

    }
}
