package com.tomato.test.map;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * ConcurrentHashMap
 *
 * @author lizhifu
 * @date 2022/5/18
 */
public class ConcurrentHashMapDemo {
    public static void main(String[] args) {
        // 初始化一个HashMap
        HashMap hashMap = new HashMap();
        hashMap.put("1", "1");
        // 初始化一个ConcurrentHashMap
        ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap(16);
        concurrentHashMap.put("1", "1");
    }
}
