package com.tomato.test.map;

/**
 * YieldDemo
 *
 * @author lizhifu
 * @date 2022/5/18
 */
public class YieldDemo {
    public static void demo() {
        int i = 0;
        int j = 0;
        while (i > 100) {
            if(j > 100) {
                Thread.yield();
                System.out.println("Thread.yield()");
            }else {
                System.out.println("执行了");
            }
        }
    }
    public static void main(String[] args) {

    }
}
