package com.tomato.test;

import com.tomato.base.dynamic.db.spring.boot.autoconfigure.properties.DynamicDataSourceContextProperties;
import com.tomato.base.dynamic.db.spring.boot.autoconfigure.properties.DynamicDataSourceProperties;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Map;

/**
 * DynamicDataSourceProperties
 *
 * @author lizhifu
 * @date 2022/3/10
 */
@SpringBootTest
public class DynamicDataSourcePropertiesTest {
    @Resource
    DynamicDataSourceProperties dynamicDataSourceProperties;

    @Test
    public void test(){
        Map<String, DynamicDataSourceContextProperties> driver = dynamicDataSourceProperties.getDynamic();
        System.out.println(dynamicDataSourceProperties.getPrimary());
        for (String s : driver.keySet()) {
            System.out.println(s);
            System.out.println(driver.get(s));
        }
    }
}
