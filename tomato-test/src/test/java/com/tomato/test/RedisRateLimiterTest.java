package com.tomato.test;


import com.tomato.base.limit.redis.executor.RateLimiterBuilder;
import com.tomato.base.limit.redis.executor.RedisRateLimiter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * RedisRateLimiter
 *
 * @author lizhifu
 * @date 2022/3/4
 */
@SpringBootTest
public class RedisRateLimiterTest {
    @Autowired
    RedisRateLimiter redisRateLimiter;

    @Test
    public void test(){
        RateLimiterBuilder rateLimiterBuilder = new RateLimiterBuilder()
                .build()
                .replenishRate(100)
                .algorithmName("TokenBucketRateLimiterAlgorithm")
                .burstCapacity(1000);
        redisRateLimiter.exe("test",rateLimiterBuilder);
    }
}
