package com.tomato.api.demo.domain;

import javax.validation.constraints.NotBlank;

/**
 * demo 实体
 *
 * @author lizhifu
 * @date 2022/3/2
 */
public class DemoEntity {
    /**
     * 姓名
     */
    @NotBlank(message = "姓名不能为空")
    private String name;
}
