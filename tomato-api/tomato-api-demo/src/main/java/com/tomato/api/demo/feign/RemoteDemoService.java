package com.tomato.api.demo.feign;

import com.tomato.base.core.constant.ServiceNameConstants;
import com.tomato.base.feign.FeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * demo 服务
 *
 * @author lizhifu
 * @date 2022/3/2
 */
@FeignClient(contextId = "remoteDemoService", value = ServiceNameConstants.DEMO_SERVICE,configuration = FeignConfig.class)
public interface RemoteDemoService {
    @GetMapping("/demo/{name}")
    public String demo(@PathVariable("name") String name);
}
