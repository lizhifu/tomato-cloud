package com.tomato.api.id.fegin;

import com.tomato.api.id.fegin.factory.RemoteIdFallbackFactory;
import com.tomato.base.core.constant.ServiceNameConstants;
import com.tomato.base.core.dto.Response;
import com.tomato.base.core.dto.SingleResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 分布式ID 服务
 *
 * @author lizhifu
 * @date 2022/3/17
 */
@FeignClient(contextId = "remoteIdService",// 当你多个接口都使用了一样的name值，则需要通过contextId来进行区分
        value = ServiceNameConstants.ID_SERVICE, //应用名，其实就是spring.application.name，用于标识某个应用，并且能从注册中心拿到对应的运行实例信息
        fallbackFactory = RemoteIdFallbackFactory.class, // 降级服务处理
        path = "/api" // 请求的前缀
)
public interface RemoteIdService {
    @RequestMapping(value = "/id/get/{bizType}")
    SingleResponse<Long> getId(@PathVariable("bizType") String bizType);
}
