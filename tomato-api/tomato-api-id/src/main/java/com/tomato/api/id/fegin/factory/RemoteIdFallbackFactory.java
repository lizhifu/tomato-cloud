package com.tomato.api.id.fegin.factory;

import com.tomato.api.id.fegin.RemoteIdService;
import com.tomato.base.core.dto.Response;
import com.tomato.base.core.dto.SingleResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * 分布式 ID 降级服务处理
 *
 * @author lizhifu
 * @date 2022/3/17
 */
@Component
public class RemoteIdFallbackFactory implements FallbackFactory<RemoteIdService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteIdFallbackFactory.class);
    @Override
    public RemoteIdService create(Throwable cause) {
        log.error("分布式 ID:{}", cause.getMessage());

        return new RemoteIdService() {
            @Override
            public SingleResponse getId(String bizType) {
                return SingleResponse.buildFailure(HttpStatus.INTERNAL_SERVER_ERROR.toString(),"分布式ID获取失败："+cause.getMessage());
            }
        };
    }
}
