package com.tomato.api.id.dataobject;

import com.tomato.base.core.dataobject.BaseDO;

import java.io.Serializable;

/**
 * ID 表 tomato-id
 *
 * @author lizhifu
 * @date 2022/3/17
 */
public class TomatoIdDO extends BaseDO implements Serializable {
    private static final long serialVersionUID = 1L;
    /** ID */
    private Long id;
    /** 当前最大id */
    private Long maxId;
    /** version */
    private Integer version;
    /** 步长 */
    private Integer step;
    /** sequence偏移量 */
    private Integer sequenceOffset;
    /** 业务类型 */
    private String bizType;
    /** 雪花算法生成的id */
    private String workId;

    public Integer getSequenceOffset() {
        return sequenceOffset;
    }

    public void setSequenceOffset(Integer sequenceOffset) {
        this.sequenceOffset = sequenceOffset;
    }

    public String getWorkId() {
        return workId;
    }

    public void setWorkId(String workId) {
        this.workId = workId;
    }

    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMaxId() {
        return maxId;
    }

    public void setMaxId(Long maxId) {
        this.maxId = maxId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    @Override
    public String toString() {
        return "TomatoId{" +
                "id=" + id +
                ", maxId=" + maxId +
                ", version=" + version +
                ", step=" + step +
                ", bizType='" + bizType + '\'' +
                '}';
    }
}
