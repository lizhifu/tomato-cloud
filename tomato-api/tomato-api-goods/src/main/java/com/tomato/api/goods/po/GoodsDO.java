package com.tomato.api.goods.po;

import com.tomato.base.core.dataobject.BaseDO;

/**
 * 商品实体
 *
 * @author lizhifu
 * @date 2022/3/22
 */
public class GoodsDO extends BaseDO {
    /**
     * 商品ID
     */
    private Long goodsId;
    /**
     * 库存
     */
    private Integer stockCount;

    /**
     * 库存剩余
     */
    private Integer stockLeftCount;
    /**
     * 商品状态：0关闭、1打开
     */
    private Integer goodsState;

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getStockCount() {
        return stockCount;
    }

    public void setStockCount(Integer stockCount) {
        this.stockCount = stockCount;
    }

    public Integer getStockLeftCount() {
        return stockLeftCount;
    }

    public void setStockLeftCount(Integer stockLeftCount) {
        this.stockLeftCount = stockLeftCount;
    }

    public Integer getGoodsState() {
        return goodsState;
    }

    public void setGoodsState(Integer goodsState) {
        this.goodsState = goodsState;
    }
}
