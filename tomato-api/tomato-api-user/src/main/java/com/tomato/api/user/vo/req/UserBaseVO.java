package com.tomato.api.user.vo.req;

import lombok.Data;

/**
 * 用户基本信息
 *
 * @author lizhifu
 * @date 2022/4/2
 */
@Data
public class UserBaseVO {

}
