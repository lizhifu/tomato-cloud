package com.tomato.api.user.exception;

import com.tomato.base.core.exception.CustomException;

/**
 * 用户异常
 *
 * @author lizhifu
 * @date 2022/4/4
 */
public class UserException extends CustomException {
    public UserException(String errMessage) {
        super(errMessage);
    }
}
