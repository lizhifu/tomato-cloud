package com.tomato.api.user.dataobject;

import com.tomato.base.core.dataobject.BaseDO;
import lombok.Data;

/**
 * account po
 *
 * @author lizhifu
 * @date 2022/3/31
 */
@Data
public class AccountDO extends BaseDO {
    /**
     * userId
     */
    private Long userId;
    /**
     * 登录账号
     */
    private String loginAccount;
}
