package com.tomato.api.user.dataobject;

import com.tomato.base.core.enums.CommonStatusEnum;
import com.tomato.base.core.dataobject.BaseDO;
import lombok.Data;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * UserPO
 *
 * @author lizhifu
 * @date 2022/3/31
 */
@Data
public class UserDO extends BaseDO {
    /**
     * userId
     */
    private Long userId;
    /**
     * 租户编号
     */
    private Long tenantId;
    /**
     * 加密后的密码
     *
     * 因为目前使用 {@link BCryptPasswordEncoder} 加密器，所以无需自己处理 salt 盐
     */
    private String password;
    /**
     * 用户类型（00系统用户）
     */
    private String userType;
    /**
     * 帐号状态
     *
     * 枚举 {@link CommonStatusEnum}
     */
    private Integer status;
}
