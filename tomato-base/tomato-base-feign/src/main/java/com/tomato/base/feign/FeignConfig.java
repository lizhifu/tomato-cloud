package com.tomato.base.feign;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * FeignConfig
 *
 * @author lizhifu
 * @date 2022/3/28
 */
@Configuration
public class FeignConfig {
    /**
     * 打印 feign 日志
     * @return
     */
    @Bean
    public Logger.Level loggerLevel(){
        // 这里记录所有，根据实际情况选择合适的日志level
        return Logger.Level.FULL;
    }
}
