package com.tomato.base.redis.redisson.bloom;

import org.redisson.api.RBloomFilter;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 布隆过滤器
 *
 * @author lizhifu
 * @date 2022/4/13
 */
public class BloomFilterService {
    @Autowired
    private RedissonClient redissonClient;

    /**
     * 创建布隆过滤器
     * @param filterName 布隆过滤器名称
     * @param expectedInsertions 布隆过滤器期望插入数量
     * @param falseProbability 布隆过滤器期望误判率
     * @param <T>
     * @return 布隆过滤器
     */
    public <T> RBloomFilter<T> create(String filterName, long expectedInsertions, double falseProbability) {
        RBloomFilter<T> bloomFilter = redissonClient.getBloomFilter(filterName);
        // 初始化布隆过滤器
        bloomFilter.tryInit(expectedInsertions, falseProbability);
        return bloomFilter;
    }
}
