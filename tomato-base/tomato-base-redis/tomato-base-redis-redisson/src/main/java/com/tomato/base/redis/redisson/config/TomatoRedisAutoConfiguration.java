package com.tomato.base.redis.redisson.config;

import com.tomato.base.redis.redisson.bloom.BloomFilterService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * Redis 配置类
 *
 * @author lizhifu
 * @date 2022/3/4
 */
@Configuration
public class TomatoRedisAutoConfiguration {

    @Bean
    public BloomFilterService bloomFilterService() {
        return new BloomFilterService();
    }
    /**
     * RedisTemplate 序列化方式
     * @param factory
     * @return
     */
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        // RedisTemplate 对象
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        // RedisConnection 工厂
        template.setConnectionFactory(factory);
        // String 序列化方式，序列化 KEY
        template.setKeySerializer(RedisSerializer.string());
        template.setHashKeySerializer(RedisSerializer.string());
        // Jackson 序列化方式
        template.setValueSerializer(RedisSerializer.json());
        template.setHashValueSerializer(RedisSerializer.json());
        return template;
    }
}
