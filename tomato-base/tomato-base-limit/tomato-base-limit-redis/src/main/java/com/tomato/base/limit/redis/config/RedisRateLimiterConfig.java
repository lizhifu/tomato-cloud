package com.tomato.base.limit.redis.config;

import com.tomato.base.limit.redis.executor.RedisRateLimiter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自定义自动配置类
 *
 * @author lizhifu
 * @date 2022/3/4
 */
@Configuration
public class RedisRateLimiterConfig {
    @Bean
    RedisRateLimiter redisRateLimiter(){
        return new RedisRateLimiter();
    }
}
