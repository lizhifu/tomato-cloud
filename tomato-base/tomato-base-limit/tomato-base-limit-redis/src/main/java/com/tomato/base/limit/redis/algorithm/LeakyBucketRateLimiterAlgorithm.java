package com.tomato.base.limit.redis.algorithm;

/**
 * 漏桶速率限制器算法
 * ⽔（请求）先进⼊到漏桶⾥，漏桶以⼀定的速度出⽔，当⽔流⼊速度过⼤会直接溢出（拒绝服务）
 * @author lizhifu
 * @date 2022/3/4
 */
public class LeakyBucketRateLimiterAlgorithm extends AbstractRateLimiterAlgorithm{
    public LeakyBucketRateLimiterAlgorithm() {
        super("leaky_bucket_rate_limiter");
    }
}
