package com.tomato.base.limit.redis.algorithm;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * 并发速率限制器算法
 * 每秒限流
 * 网关每秒限流
 * IP每秒限流
 * @author lizhifu
 * @date 2022/3/4
 */
public class ConcurrentRateLimiterAlgorithm extends AbstractRateLimiterAlgorithm{
    public ConcurrentRateLimiterAlgorithm() {
        super("concurrent_rate_limiter");
    }

    /**
     * key 需要特殊定制
     * @param key
     * @return
     */
    @Override
    public List<String> getKeys(final String key) {
        String tokenKey = getScriptName() + ".{" + key + "}.tokens";
        // 随机数
        String requestKey = UUID.randomUUID().toString().replace("-","");
        return Arrays.asList(tokenKey, requestKey);
    }
}
