package com.tomato.base.limit.redis.algorithm;

/**
 * 令牌桶速率限制器算法
 * 系统以恒定的速率产⽣令牌，然后将令牌放⼊令牌桶中
 * 令牌桶有⼀个容量，当令牌桶满了的时候，再向其中放⼊的令牌就会被丢弃。
 * 每次⼀个请求过来，需要从令牌桶中获取⼀个令牌，如果有令牌，则提供服务；如果没有令牌，则拒绝服务。
 * @author lizhifu
 * @date 2022/3/4
 */
public class TokenBucketRateLimiterAlgorithm extends AbstractRateLimiterAlgorithm{
    public TokenBucketRateLimiterAlgorithm() {
        super("token_bucket_limiter");
    }
}