package com.tomato.base.limit.redis.executor;

import com.tomato.base.limit.redis.algorithm.RateLimiterAlgorithm;
import com.tomato.base.limit.redis.em.AlgorithmTypeEnum;

/**
 * 限流参数 builder
 *
 * @author lizhifu
 * @date 2022/3/4
 */
public class RateLimiterBuilder {
    /**
     * 算法
     */
    private String algorithmName;

    /**
     * 补充速率
     */
    private double replenishRate;

    /**
     * 总容量
     */
    private double burstCapacity;

    /**
     * 请求数量
     */
    private double requestCount = 1.0;
    /**
     * 算法
     */
    private RateLimiterAlgorithm rateLimiterAlgorithm;

    public RateLimiterBuilder build() {
        return new RateLimiterBuilder();
    }
    public RateLimiterBuilder requestCount(double requestCount) {
        this.requestCount = requestCount;
        return this;
    }
    public RateLimiterBuilder replenishRate(double replenishRate) {
        this.replenishRate = replenishRate;
        return this;
    }
    public RateLimiterBuilder burstCapacity(double burstCapacity) {
        this.burstCapacity = burstCapacity;
        return this;
    }
    public RateLimiterBuilder algorithmName(String algorithmName) {
        rateLimiterAlgorithm = AlgorithmTypeEnum.buildRateLimiterAlgorithm(algorithmName);
        return this;
    }

    public String getAlgorithmName() {
        return algorithmName;
    }

    public double getReplenishRate() {
        return replenishRate;
    }

    public double getBurstCapacity() {
        return burstCapacity;
    }

    public double getRequestCount() {
        return requestCount;
    }

    public RateLimiterAlgorithm getRateLimiterAlgorithm() {
        return rateLimiterAlgorithm;
    }
}
