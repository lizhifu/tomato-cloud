package com.tomato.base.limit.redis.algorithm;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * 滑动窗口速率限制器算法
 * 滑动时间窗口通过维护⼀个单位时间内的计数值，每当⼀个请求通过时，就将计数值加1
 * 当计数值超过预先设定的阈值时，就拒绝单位时间内的其他请求。如果单位时间已经结束，则将计数器清零，开启下⼀轮的计数。
 * @author lizhifu
 * @date 2022/3/4
 */
public class SlidingWindowRateLimiterAlgorithm extends AbstractRateLimiterAlgorithm{
    public SlidingWindowRateLimiterAlgorithm() {
        super("sliding_window_rate_limiter");
    }

    /**
     * key 需要特殊定制
     * @param key
     * @return
     */
    @Override
    public List<String> getKeys(final String key) {
        String prefix = getScriptName() + ".{" + key;
        String tokenKey = prefix + "}.tokens";
        // 随机数
        String requestKey = UUID.randomUUID().toString().replace("-","");
        return Arrays.asList(tokenKey, requestKey);
    }
}
