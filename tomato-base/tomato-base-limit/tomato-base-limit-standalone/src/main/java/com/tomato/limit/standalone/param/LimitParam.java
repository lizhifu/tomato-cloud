package com.tomato.limit.standalone.param;

import java.io.Serializable;

/**
 * 计数数据
 * TODO
 * @author lizhifu
 * @date 2022/3/4
 */
public class LimitParam implements Serializable {
    /**
     * 最大访问数量
     */
    private int limit = 10;
    /**
     * 访问时间差
     */
    private long timeout = 1000;
    /**
     * 访问 key
     */
    private String key;

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
