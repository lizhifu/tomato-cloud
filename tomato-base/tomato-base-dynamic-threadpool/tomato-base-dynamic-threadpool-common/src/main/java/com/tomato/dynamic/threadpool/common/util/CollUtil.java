package com.tomato.dynamic.threadpool.common.util;

import java.util.Collection;
import java.util.Map;

/**
 * 集合工具类
 *
 * @author lizhifu
 * @date 2022/4/11
 */
public class CollUtil {
    /**
     * 判断集合是否为空
     * @param collection
     * @return
     */
    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }
    public static boolean isNotEmpty(Collection<?> collection) {
        return null != collection && !collection.isEmpty();
    }
}
