package com.tomato.dynamic.threadpool.common.config;

import com.tomato.dynamic.threadpool.common.enums.QueueTypeEnum;
import com.tomato.dynamic.threadpool.common.enums.RejectedTypeEnum;
import lombok.Data;

import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * ThreadPool main properties.
 *
 * @author: yanhom
 * @since 1.0.0
 **/
@Data
public class ThreadPoolProperties {

    /**
     * 线程池名称
     */
    private String threadPoolName = "dynamic-thread-pool";

    /**
     * Executor type, used in create phase.
     */
    private String executorType;

    /**
     * 核心线程数量
     */
    private int corePoolSize = 1;

    /**
     * 最大线程数量
     */
    private int maximumPoolSize = Runtime.getRuntime().availableProcessors();;

    /**
     * 队列默认最大容量
     */
    private int queueCapacity = 512;

    /**
     * 队列类型 {@link QueueTypeEnum}
     */
    private String queueType = QueueTypeEnum.VARIABLE_LINKED_BLOCKING_QUEUE.getName();


    /**
     * 拒绝类型 {@link RejectedTypeEnum}
     */
    private String rejectedHandlerType = RejectedTypeEnum.ABORT_POLICY.getName();

    /**
     * 当线程池中线程数量大于corePoolSize（核心线程数量）或设置了allowCoreThreadTimeOut（是否允许空闲核心线程超时）时，
     * 线程会根据keepAliveTime的值进行活性检查，一旦超时便销毁线程。
     */
    private long keepAliveTime = 30;

    /**
     * 单位
     */
    private TimeUnit unit = TimeUnit.SECONDS;

    /**
     * 允许核心线程超时
     */
    private boolean allowCoreThreadTimeOut = false;

    /**
     * 线程名称前缀
     */
    private String threadNamePrefix = "dynamic-thread";

    /**
     * 不中断正在运行的任务并执行队列中的所有任务
     */
    private boolean waitForTasksToCompleteOnShutdown = false;

    /**
     *  awaitTerminationSeconds 等待时间
     */
    private int awaitTerminationSeconds = 0;

    /**
     * 如果预先启动所有核心线程。
     */
    private boolean preStartAllCoreThreads = false;

    /**
     * 任务执行超时阈值
     */
    private long runTimeoutThreshold = 0;

    /**
     * 任务在队列等待超时阈值
     */
    private long queueTimeoutThreshold = 0;

    /**
     * Task wrapper names.
     */
    private Set<String> taskWrapperNames;
}
