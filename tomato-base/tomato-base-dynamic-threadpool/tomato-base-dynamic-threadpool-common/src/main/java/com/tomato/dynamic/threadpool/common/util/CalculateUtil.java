package com.tomato.dynamic.threadpool.common.util;

/**
 * 计算
 * @author lizhifu
 */
public class CalculateUtil {

    /**
     * 除
     *
     * @param num1
     * @param num2
     * @return
     */
    public static int divide(int num1, int num2) {
        return ((int) (Double.parseDouble(num1 + "") / Double.parseDouble(num2 + "") * 100));
    }

}
