package com.tomato.dynamic.threadpool.common.queue;

import lombok.Data;
import lombok.Getter;

/**
 * 队列信息
 *
 * @author lizhifu
 * @date 2022/4/8
 */
@Data
public class BlockingQueueInfo {
    /**
     * 队列元素个数
     */
    private int queueSize;
    /**
     * 队列剩余容量
     */
    private int remainingCapacity;
    /**
     * 队列容量
     */
    private int queueCapacity;
    /**
     * 队列名称
     */
    private String queueName;
}
