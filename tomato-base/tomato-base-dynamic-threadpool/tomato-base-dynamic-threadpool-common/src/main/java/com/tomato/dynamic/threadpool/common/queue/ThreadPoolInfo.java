package com.tomato.dynamic.threadpool.common.queue;

import lombok.Data;

/**
 * 线程池信息
 *
 * @author lizhifu
 * @date 2022/4/8
 */
@Data
public class ThreadPoolInfo {
    /**
     * 核心线程数
     */
    private int corePoolSize;
    /**
     * 最大线程数
     */
    private int maximumPoolSize;
    /**
     * 线程池当前线程数 (有锁)
     */
    private int poolSize;

    /**
     * 活跃线程数 (有锁)
     */
    private int activeCount;

    /**
     * 同时进入池中的最大线程数 (有锁)
     */
    private int largestPoolSize;

    /**
     * 线程池中执行任务总数量 (有锁)
     */
    private long completedTaskCount;

    /**
     * 当前负载
     */
    private String currentLoad;

    /**
     * 峰值负载
     */
    private String peakLoad;
    /**
     * 拒绝次数
     */
    private Long rejectCount;
    /**
     * 拒绝策略名称
     */
    private String rejectedExecutionHandlerName;

    /**
     * 执行超时任务数量
     */
    private int runTimeoutCount;

    /**
     * 在队列等待超时任务数量
     */
    private int queueTimeoutCount;
}
