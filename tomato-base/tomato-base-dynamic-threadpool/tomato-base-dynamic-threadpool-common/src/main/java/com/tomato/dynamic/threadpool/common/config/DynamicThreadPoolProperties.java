package com.tomato.dynamic.threadpool.common.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * 动态线程池配置
 *
 * @author lizhifu
 * @date 2022/4/8
 */
@Slf4j
@Data
@ConfigurationProperties(prefix = DynamicThreadPoolProperties.PROPERTIES_PREFIX)
public class DynamicThreadPoolProperties {
    public static final String PROPERTIES_PREFIX = "spring.dynamic.threadpool";
    public static final String DYNAMIC_ENABLED = PROPERTIES_PREFIX + ".enabled";

    /**
     * 线程池监控，默认 5s
     */
    private int monitorInterval = 5;

    /**
     * 线程池配置
     */
    private List<ThreadPoolProperties> executors;
}
