package com.tomato.dynamic.threadpool.common.constant;

/**
 * 参数常量
 *
 * @author lizhifu
 * @date 2022/4/11
 */
public class DynamicThreadPoolConstant {
    public static final String THREAD_POOL_NAME = "threadPoolName";

    public static final String ALLOW_CORE_THREAD_TIMEOUT = "allowCoreThreadTimeOut";

    public static final String NOTIFY_ITEMS = "notifyItems";

    public static final String WAIT_FOR_TASKS_TO_COMPLETE_ON_SHUTDOWN = "waitForTasksToCompleteOnShutdown";

    public static final String AWAIT_TERMINATION_SECONDS = "awaitTerminationSeconds";

    public static final String PRE_START_ALL_CORE_THREADS = "preStartAllCoreThreads";

    public static final String RUN_TIMEOUT_THRESHOLD = "runTimeoutThreshold";

    public static final String QUEUE_TIMEOUT_THRESHOLD = "queueTimeoutThreshold";

    public static final String TASK_WRAPPERS = "taskWrappers";
}
