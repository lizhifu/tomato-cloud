package com.tomato.dynamic.threadpool.common.enums;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 拒绝策略枚举
 * @author lizhifu
 */
@Slf4j
@Getter
public enum RejectedTypeEnum {

    /**
     * 被拒绝任务的处理程序, 抛出异常
     */
    ABORT_POLICY("AbortPolicy"),
    /**
     * 被拒绝任务的程序由主线程执行
     */
    CALLER_RUNS_POLICY("CallerRunsPolicy"),
    /**
     * 被拒绝任务的处理程序, 它丢弃最早的未处理请求, 然后重试
     */
    DISCARD_OLDEST_POLICY("DiscardOldestPolicy"),
    /**
     * 被拒绝任务的处理程序, 默默地丢弃被拒绝的任务。
     */
    DISCARD_POLICY("DiscardPolicy");

    private final String name;

    RejectedTypeEnum(String name) {
        this.name = name;
    }
}
