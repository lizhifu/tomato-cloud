package com.tomato.dynamic.threadpool.common.enums;

import lombok.Getter;

/**
 * 报警类型、通知类型
 * @author lizhifu
 */
@Getter
public enum NotifyTypeEnum {

    /**
     * 线程池配置变更
     */
    CHANGE("change"),

    /**
     * 线程池活跃度 = 线程池中线程数 / 线程池最大线程数
     */
    ACTIVITY("activity"),

    /**
     * 队列容量 = 队列中任务数 / 队列最大任务数
     */
    CAPACITY("capacity"),

    /**
     * 拒绝次数
     */
    REJECT("reject"),

    /**
     * 任务执行超时次数
     */
    RUN_TIMEOUT("run_timeout"),

    /**
     * 队列排队时间过长次数
     */
    QUEUE_TIMEOUT("queue_timeout");

    private final String value;

    NotifyTypeEnum(String value) {
        this.value = value;
    }

    public static NotifyTypeEnum of(String value) {
        for (NotifyTypeEnum typeEnum : NotifyTypeEnum.values()) {
            if (typeEnum.value.equals(value)) {
                return typeEnum;
            }
        }
        return null;
    }
}
