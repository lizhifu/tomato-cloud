package com.tomato.dynamic.threadpool.core.support;

/**
 * 扩展 Runnable 接口，提供线程池管理功能
 *
 * @author lizhifu
 * @date 2022/4/11
 */
public class DynamicThreadPoolRunnable implements Runnable {
    private final Runnable runnable;

    private final Long submitTime;

    private Long startTime;

    public DynamicThreadPoolRunnable(Runnable runnable) {
        this.runnable = runnable;
        this.submitTime = System.currentTimeMillis();
    }

    @Override
    public void run() {
        runnable.run();
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getSubmitTime() {
        return submitTime;
    }

    public Long getStartTime() {
        return startTime;
    }
}
