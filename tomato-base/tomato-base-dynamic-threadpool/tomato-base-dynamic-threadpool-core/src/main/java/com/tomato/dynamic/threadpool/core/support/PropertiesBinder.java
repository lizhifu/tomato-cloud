package com.tomato.dynamic.threadpool.core.support;

import com.tomato.dynamic.threadpool.common.config.DynamicThreadPoolProperties;
import org.springframework.boot.context.properties.bind.Bindable;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.boot.context.properties.source.ConfigurationPropertySource;
import org.springframework.boot.context.properties.source.MapConfigurationPropertySource;
import org.springframework.core.ResolvableType;
import org.springframework.core.env.Environment;

import java.util.Map;

/**
 * PropertiesBinder related
 *
 * @author: yanhom
 * @since 1.0.3
 **/
public class PropertiesBinder {

    private PropertiesBinder() {}

    public static void bindDtpProperties(Map<Object, Object> properties, DynamicThreadPoolProperties dynamicThreadPoolProperties) {
        ConfigurationPropertySource sources = new MapConfigurationPropertySource(properties);
        Binder binder = new Binder(sources);
        ResolvableType type = ResolvableType.forClass(DynamicThreadPoolProperties.class);
        Bindable<?> target = Bindable.of(type).withExistingValue(dynamicThreadPoolProperties);
        binder.bind(DynamicThreadPoolProperties.PROPERTIES_PREFIX, target);
    }

    public static void bindDtpProperties(Environment environment, DynamicThreadPoolProperties dynamicThreadPoolProperties) {
        Binder binder = Binder.get(environment);
        ResolvableType type = ResolvableType.forClass(DynamicThreadPoolProperties.class);
        Bindable<?> target = Bindable.of(type).withExistingValue(dynamicThreadPoolProperties);
        binder.bind(DynamicThreadPoolProperties.PROPERTIES_PREFIX, target);
    }
}
