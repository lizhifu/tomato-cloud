package com.tomato.dynamic.threadpool.core.support.wrapper;

import lombok.Data;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * ThreadPoolExecutor 包装类
 */
@Data
public class ExecutorWrapper {

    private String threadPoolName;

    private ThreadPoolExecutor executor;
}
