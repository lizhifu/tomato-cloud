package com.tomato.dynamic.threadpool.core.manage;

import com.tomato.dynamic.threadpool.common.enums.NotifyTypeEnum;
import com.tomato.dynamic.threadpool.core.thread.DynamicThreadPoolExecutor;
import lombok.extern.slf4j.Slf4j;

/**
 * 通知管理器
 *
 * @author lizhifu
 * @date 2022/4/11
 */
@Slf4j
public class AlarmManager {
    public static void doAlarm(DynamicThreadPoolExecutor executor, NotifyTypeEnum typeEnum) {
        log.info("DynamicThreadPoolExecutor 通知, 线程池名称: {}, 通知类型: {}", executor.getThreadPoolName(), typeEnum.getValue());
    }
}
