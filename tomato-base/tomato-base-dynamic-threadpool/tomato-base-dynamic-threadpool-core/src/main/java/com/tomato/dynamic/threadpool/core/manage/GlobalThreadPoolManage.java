package com.tomato.dynamic.threadpool.core.manage;

import com.tomato.dynamic.threadpool.core.thread.DynamicThreadPoolExecutor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 线程池统一管理
 * @author lizhifu
 */
@Slf4j
public class GlobalThreadPoolManage {

    /**
     * 动态线程池容器.
     */
    private static final Map<String, DynamicThreadPoolExecutor> EXECUTOR_MAP = new ConcurrentHashMap();

    /**
     * 获取动态线程池类.
     *
     * @param threadPoolName
     * @return
     */
    public static DynamicThreadPoolExecutor getExecutorService(String threadPoolName) {
        return EXECUTOR_MAP.get(threadPoolName);
    }
    /**
     * 注册动态线程池.
     *
     * @param threadPoolName
     * @param executor
     */
    public static void registerPool(String threadPoolName, DynamicThreadPoolExecutor executor) {
        log.info("GlobalThreadPoolManage registerPool threadPoolName: {}", threadPoolName);
        EXECUTOR_MAP.put(threadPoolName, executor);
    }

    /**
     * 获取所有的线程池
     * @return
     */
    public static List<String> list() {
        return new ArrayList<>(EXECUTOR_MAP.keySet());
    }
}
