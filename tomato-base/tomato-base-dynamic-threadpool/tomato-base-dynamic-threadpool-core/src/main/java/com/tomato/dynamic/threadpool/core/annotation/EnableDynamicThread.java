package com.tomato.dynamic.threadpool.core.annotation;

import com.tomato.dynamic.threadpool.core.DynamicThreadBeanDefinitionRegistrar;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 是否开启动态线程池
 *
 * @author lizhifu
 * @date 2022/4/11
 */
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(DynamicThreadBeanDefinitionRegistrar.class)
public @interface EnableDynamicThread {
}
