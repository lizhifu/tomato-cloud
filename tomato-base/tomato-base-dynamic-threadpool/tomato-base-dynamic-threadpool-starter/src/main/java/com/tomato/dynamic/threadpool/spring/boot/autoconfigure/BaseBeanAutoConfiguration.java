package com.tomato.dynamic.threadpool.spring.boot.autoconfigure;

import com.tomato.dynamic.threadpool.common.ApplicationContextHolder;
import com.tomato.dynamic.threadpool.common.config.DynamicThreadPoolProperties;
import com.tomato.dynamic.threadpool.core.monitor.DynamicThreadPoolMonitor;
import com.tomato.dynamic.threadpool.spring.boot.autoconfigure.support.DynamicThreadPoolBeanPostProcessor;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import static com.tomato.dynamic.threadpool.common.config.DynamicThreadPoolProperties.DYNAMIC_ENABLED;

/**
 * spring boot 基础bean自动配置
 *
 * @author lizhifu
 * @date 2022/4/8
 */
@Configuration
@AllArgsConstructor
@EnableConfigurationProperties(DynamicThreadPoolProperties.class)
@ConditionalOnProperty(name = DYNAMIC_ENABLED, matchIfMissing = true, havingValue = "true")
public class BaseBeanAutoConfiguration {
    private final DynamicThreadPoolProperties dynamicThreadPoolProperties;
    @Bean
    public ApplicationContextHolder dynamicThreadApplicationContextHolder() {
        return new ApplicationContextHolder();
    }

    @Bean
    @DependsOn({"dynamicThreadApplicationContextHolder"})
    @ConditionalOnMissingBean
    public DynamicThreadPoolBeanPostProcessor dynamicThreadPoolPostProcessor() {
        return new DynamicThreadPoolBeanPostProcessor(dynamicThreadPoolProperties);
    }
    @Bean
    @ConditionalOnMissingBean
    public DynamicThreadPoolMonitor dynamicThreadPoolMonitor() {
        return new DynamicThreadPoolMonitor();
    }
}
