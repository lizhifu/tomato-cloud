package com.tomato.base.controller.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 跨域配置
 *
 * @author lizhifu
 * @data 2021年09月14日
 */
@Configuration
public class DefaultWebMvcConfigurer implements WebMvcConfigurer {
    private Logger log = LoggerFactory.getLogger(DefaultWebMvcConfigurer.class);
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        log.info("CorsConfig 配置");
        registry.addMapping("/**")
                //spring2.4版本不能使用 allowedOrigins
                .allowedOriginPatterns("*")
                .allowedMethods("GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS")
                .allowCredentials(true)
                .maxAge(3600)
                .allowedHeaders("*");
    }

    /**
     * No mapping for GET /favicon.ico
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/static/")
                .addResourceLocations("classpath:/META-INF/resources/");
    }
}
