package com.tomato.base.controller.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcRegistrations;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * WebMvcConfig
 * 不要使用 WebMvcConfigurationSupport
 * @author lizhifu
 * @date 2021/12/10
 */
@Configuration
public class WebConfig implements WebMvcRegistrations {
    private Logger log = LoggerFactory.getLogger(WebConfig.class);
    @Override
    public RequestMappingHandlerMapping getRequestMappingHandlerMapping() {
        log.info("RequestMappingHandlerMapping 配置");
        return new CustomRequestMappingHandlerMapping();
    }
}
