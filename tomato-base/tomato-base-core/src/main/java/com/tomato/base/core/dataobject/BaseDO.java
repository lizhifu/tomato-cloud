package com.tomato.base.core.dataobject;

import java.io.Serializable;
import java.util.Date;

/**
 * BasePO
 *
 * @author lizhifu
 * @date 2022/3/22
 */
public class BaseDO implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 自增ID */
    private Long id;
    /** 创建时间 */
    private Date createTime;
    /** 更新时间 */
    private Date updateTime;
}
