package com.tomato.base.core.constant;

/**
 * 商品状态
 *
 * @author lizhifu
 * @date 2022/3/22
 */
public enum GoodsState {
    CLOSED(0, "关闭"),
    OPEN(1, "打开");
    private Integer code;
    private String info;

    GoodsState(Integer code, String info) {
        this.code = code;
        this.info = info;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

}
