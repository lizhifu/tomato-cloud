package com.tomato.base.core.constant;

/**
 * 服务名称
 *
 * @author lizhifu
 * @date 2022/3/2
 */
public class ServiceNameConstants {
    /**
     * demo 模块
     */
    public static final String DEMO_SERVICE = "tomato-demo";
    /**
     * 分布式 ID 模块
     */
    public static final String ID_SERVICE = "tomato-id";
}
