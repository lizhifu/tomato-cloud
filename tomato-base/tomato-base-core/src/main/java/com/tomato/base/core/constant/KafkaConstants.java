package com.tomato.base.core.constant;

/**
 * KafkaConstants
 *
 * @author lizhifu
 * @date 2022/3/22
 */
public class KafkaConstants {
    /**
     * topics 主题
     */
    public static final String TOPIC_GOODS = "Hello-Kafka";
    /**
     * groupId 组
     */
    public static final String TOPIC_GROUP = "test-consumer-group";
}
