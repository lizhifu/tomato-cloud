package com.tomato.base.core.constant;

/**
 * 分布式ID
 *
 * @author lizhifu
 * @date 2022/4/1
 */
public class IdNameConstants {
    /**
     * tomato-modules-user 模块
     */
    public static final String TOMATO_MODULES_USER = "tomato-modules-user";
}
