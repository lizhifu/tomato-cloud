# 简单的分库分表
## 水平拆分
把一个表的数据给弄到多个库的多个表里去，但是每个库的表结构都一样，只不过每个库表放的数据是不同的，所有库表的数据加起来就是全部数据。


参考：

https://gitee.com/baomidou/dynamic-datasource-spring-boot-starter