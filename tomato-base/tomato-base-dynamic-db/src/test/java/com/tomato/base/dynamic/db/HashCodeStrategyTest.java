package com.tomato.base.dynamic.db;

/**
 * HashCodeStrategy
 *
 * @author lizhifu
 * @date 2022/3/6
 */
public class HashCodeStrategyTest {
    public static void main(String[] args) {
        String dynamicKey = "144422298005";
        for (int i = 0; i < 1000; i++) {
            dynamicKey = dynamicKey + i;
            // 学习 HashMap 的原理，类似于整个数组大小为 库数量 *  表数量
            // 整个库表总量为：库数量 *  表数量
            // 2 个库，4 个表，那么总量为 2 * 4 = 8
            int size = 3 * 1;

            // 扰动函数 : 增加随机性，让数据元素更加均匀的散列，减少碰撞
            int idx = (size - 1) & (dynamicKey.hashCode() ^ (dynamicKey.hashCode() >>> 16));
            System.out.println(String.format("%02d", idx));

            // idx 即为扰动函数
            // 库1   库2
            // 1111 1111
            // idx 为 3 为库1表3
            // idx 为 7 为库2表3
            int dbIdx = idx / 1 + 1;
//            String dbKey = String.format("%02d", dbIdx);
//            System.out.println(dbKey);
        }
    }
}
