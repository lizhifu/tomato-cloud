package com.tomato.base.dynamic.db.annotation;

import java.lang.annotation.*;

/**
 * 动态数据源注解
 *
 * @author lizhifu
 * @date 2021/9/13
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DynamicDataSourceAnnotation {
    /** 分库字段，空必须指定特定数据源 */
    String key() default "";

    /**
     * 指定数据源
     * @return
     */
    String specialDataSource() default "";

    /**
     * 分库策略，默认取模
     * @return
     */
    String strategy() default "MoldDataSourceStrategy";
}
