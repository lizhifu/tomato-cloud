package com.tomato.base.dynamic.db.strategy;

/**
 * 分表策略
 *
 * @author lizhifu
 * @date 2021/12/1
 */
public interface DynamicTableStrategy<T> extends BaseStrategy<T>{

}
