package com.tomato.base.dynamic.db.spring.boot.autoconfigure.properties;

import lombok.Getter;
import lombok.Setter;

/**
 * DynamicDataSourceContextProperties
 *
 * @author lizhifu
 * @date 2022/3/10
 */
@Getter
@Setter
public class DynamicDataSourceContextProperties {
    /**
     * JDBC driver
     */
    private String driverClassName;
    /**
     * JDBC url 地址
     */
    private String url;
    /**
     * JDBC 用户名
     */
    private String username;
    /**
     * JDBC 密码
     */
    private String password;
}
