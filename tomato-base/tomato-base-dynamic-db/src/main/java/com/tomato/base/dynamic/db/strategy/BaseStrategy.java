package com.tomato.base.dynamic.db.strategy;

/**
 * BaseStrategy
 *
 * @author lizhifu
 * @date 2022/3/13
 */
public interface BaseStrategy<T> {
    /**
     * 清除路由
     */
    void clear();

    /**
     * 分表
     */
    void dynamic(T t);
}
