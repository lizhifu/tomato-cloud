package com.tomato.base.dynamic.db.strategy.impl;

import com.tomato.base.dynamic.db.holder.DynamicTableContextHolder;
import com.tomato.base.dynamic.db.spring.boot.autoconfigure.properties.DynamicTableProperties;
import com.tomato.base.dynamic.db.strategy.AbstractDynamicTableStrategy;
import lombok.extern.slf4j.Slf4j;

/**
 * 取模算法
 *
 * @author lizhifu
 * @date 2022/4/6
 */
@Slf4j
public class MoldTableStrategy extends AbstractDynamicTableStrategy<String> {

    public MoldTableStrategy(DynamicTableProperties dynamicTableProperties) {
        super(dynamicTableProperties);
    }

    @Override
    public void dynamicTable(String s) {
        int dbKey = Integer.valueOf(s) % super.dynamicTableProperties.getCount();
        log.info("MoldTableStrategy 表路由 dbKey：{} ", dbKey);
        DynamicTableContextHolder.setTable(super.dynamicTableProperties.getSuffix() + dbKey);
    }
}
