package com.tomato.base.dynamic.db.enums;

import com.tomato.base.dynamic.db.spring.boot.autoconfigure.properties.DynamicDataSourceProperties;
import com.tomato.base.dynamic.db.spring.boot.autoconfigure.properties.DynamicTableProperties;
import com.tomato.base.dynamic.db.strategy.BaseStrategy;
import com.tomato.base.dynamic.db.strategy.impl.*;

import java.util.Objects;

/**
 * 算法类型
 *
 * @author lizhifu
 * @date 2022/4/1
 */
public enum StrategyTypeEnum {
    /**
     * 分库
     */
    MOLD_DATASOURCE_STRATEGY("MoldDataSourceStrategy"),
    /**
     * 分库
     */
    DATE_DYNAMIC_DATASOURCE_STRATEGY("DateDynamicDataSourceStrategy"),
    /**
     * 分库
     */
    SPECIAL_DATASOURCE_STRATEGY("SpecialDataSourceStrategy"),
    /**
     * 分库
     */
    HASHCODE_DATASOURCE_STRATEGY("HashCodeDataSourceStrategy"),
    /**
     * 分表
     */
    DATE_DYNAMIC_TABLE_STRATEGY("DateDynamicTableStrategy"),
    /**
     * 分表
     */
    MOLD_TABLE_STRATEGY("MoldTableStrategy");
    /**
     * name
     */
    private final String name;
    StrategyTypeEnum(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public static BaseStrategy buildBaseStrategy(String name, DynamicDataSourceProperties dynamicDataSourceProperties) {
        BaseStrategy baseStrategy = null;
        if (Objects.equals(name, DATE_DYNAMIC_DATASOURCE_STRATEGY.getName())) {
            baseStrategy = new DateDynamicDataSourceStrategy(dynamicDataSourceProperties);
        }
        else if (Objects.equals(name, SPECIAL_DATASOURCE_STRATEGY.getName())) {
            baseStrategy = new SpecialDataSourceStrategy(dynamicDataSourceProperties);
        }
        else if (Objects.equals(name, MOLD_DATASOURCE_STRATEGY.getName())) {
            baseStrategy = new MoldDataSourceStrategy(dynamicDataSourceProperties);
        }
        if (baseStrategy != null) {
            return baseStrategy;
        }
        throw new RuntimeException("不支持的算法类型： " + name);
    }

    public static BaseStrategy buildTableBaseStrategy(String name, DynamicTableProperties dynamicTableProperties) {
        BaseStrategy baseStrategy = null;
        if (Objects.equals(name, DATE_DYNAMIC_DATASOURCE_STRATEGY.getName())) {
        }
        else if (Objects.equals(name, MOLD_TABLE_STRATEGY.getName())) {
            baseStrategy = new MoldTableStrategy(dynamicTableProperties);
        }
        if (baseStrategy != null) {
            return baseStrategy;
        }
        throw new RuntimeException("不支持的算法类型： " + name);
    }
}
