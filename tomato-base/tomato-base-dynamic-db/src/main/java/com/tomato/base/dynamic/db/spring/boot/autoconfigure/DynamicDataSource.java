package com.tomato.base.dynamic.db.spring.boot.autoconfigure;

import com.tomato.base.dynamic.db.holder.DynamicDataSourceContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 动态数据源获取
 *
 * @author lizhifu
 * @date 2022/3/9
 */
@Slf4j
public class DynamicDataSource extends AbstractRoutingDataSource {
    @Override
    protected Object determineCurrentLookupKey() {
        // "ds" +
        String db = DynamicDataSourceContextHolder.getDataSource();
        return db;
    }
}
