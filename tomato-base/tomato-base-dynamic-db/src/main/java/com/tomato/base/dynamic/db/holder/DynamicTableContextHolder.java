package com.tomato.base.dynamic.db.holder;

/**
 * 动态分表上下文
 *
 * @author lizhifu
 * @date 2021/9/8
 */
public class DynamicTableContextHolder {
    /**
     * 当前线程分表的key
     */
    private static final ThreadLocal<String> CURRENT_TABLE_HOLDER = new ThreadLocal<String>();

    /**
     * 切换分表
     * @param Table
     */
    public static void setTable(String Table) {
        CURRENT_TABLE_HOLDER.set(Table);
    }

    /**
     * 获取分表
     * @return
     */
    public static String getTable() {
        return CURRENT_TABLE_HOLDER.get();
    }

    /**
     * 清空分表
     */
    public static void clearTable() {
        CURRENT_TABLE_HOLDER.remove();
    }
}
