package com.tomato.base.dynamic.db.strategy;


import com.tomato.base.dynamic.db.holder.DynamicTableContextHolder;
import com.tomato.base.dynamic.db.spring.boot.autoconfigure.properties.DynamicTableProperties;

/**
 * 抽象实现
 *
 * @author lizhifu
 * @date 2022/3/13
 */
public abstract class AbstractDynamicTableStrategy<T> implements DynamicTableStrategy<T> {
    protected DynamicTableProperties dynamicTableProperties;
    public AbstractDynamicTableStrategy(DynamicTableProperties dynamicTableProperties){
        this.dynamicTableProperties = dynamicTableProperties;
    }
    @Override
    public void clear() {
        DynamicTableContextHolder.clearTable();
    }

    @Override
    public void dynamic(T t){
        dynamicTable(t);
    }
    public abstract void dynamicTable(T t);
}
