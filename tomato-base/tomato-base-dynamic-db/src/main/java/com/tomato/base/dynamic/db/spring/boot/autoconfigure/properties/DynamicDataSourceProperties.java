package com.tomato.base.dynamic.db.spring.boot.autoconfigure.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;
import java.util.Map;

/**
 * 动态数据源配置
 *
 * @author lizhifu
 * @date 2022/3/9
 */
@ConfigurationProperties(prefix = "spring.datasource")
@Getter
@Setter
public class DynamicDataSourceProperties {
    /**
     * 默认的库,默认master
     */
    private String primary = "master";
    /**
     * 分库数量：默认 1
     */
    private int dbCount = 1;
    /**
     * 分库配置前缀
     */
    private String prefix;
    /**
     * 分库分表配置
     */
    private Map<String, DynamicTableProperties> table;

    private Map<String,DynamicDataSourceContextProperties> dynamic;

}
