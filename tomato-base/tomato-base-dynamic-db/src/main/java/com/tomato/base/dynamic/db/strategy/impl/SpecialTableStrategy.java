package com.tomato.base.dynamic.db.strategy.impl;

import com.tomato.base.dynamic.db.holder.DynamicTableContextHolder;
import com.tomato.base.dynamic.db.spring.boot.autoconfigure.properties.DynamicDataSourceProperties;
import com.tomato.base.dynamic.db.spring.boot.autoconfigure.properties.DynamicTableProperties;
import com.tomato.base.dynamic.db.strategy.AbstractDynamicTableStrategy;

/**
 * 指定数据源
 *
 * @author lizhifu
 * @date 2022/3/13
 */
public class SpecialTableStrategy extends AbstractDynamicTableStrategy<String> {
    public SpecialTableStrategy(DynamicTableProperties dynamicTableProperties) {
        super(dynamicTableProperties);
    }

    @Override
    public void dynamicTable(String table) {
        DynamicTableContextHolder.setTable(table);
    }
}
