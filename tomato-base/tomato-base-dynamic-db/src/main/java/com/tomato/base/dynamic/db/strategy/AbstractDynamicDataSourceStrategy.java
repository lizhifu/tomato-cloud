package com.tomato.base.dynamic.db.strategy;

import com.tomato.base.dynamic.db.holder.DynamicDataSourceContextHolder;
import com.tomato.base.dynamic.db.spring.boot.autoconfigure.properties.DynamicDataSourceProperties;

/**
 * 抽象实现
 *
 * @author lizhifu
 * @date 2022/3/13
 */
public abstract class AbstractDynamicDataSourceStrategy<T> implements DynamicDataSourceStrategy<T> {
    protected DynamicDataSourceProperties dynamicDataSourceProperties;
    public AbstractDynamicDataSourceStrategy(DynamicDataSourceProperties dynamicDataSourceProperties){
        this.dynamicDataSourceProperties = dynamicDataSourceProperties;
    }
    @Override
    public void clear() {
        DynamicDataSourceContextHolder.clearDataSource();
    }
    @Override
    public void dynamic(T t){
        dynamicDataSource(t);
    }
    /**
     * 数据源
     *
     * @param t 标识
     */
    public abstract void dynamicDataSource(T t);
}
