package com.tomato.base.dynamic.db.strategy.impl;

import com.tomato.base.dynamic.db.holder.DynamicDataSourceContextHolder;
import com.tomato.base.dynamic.db.spring.boot.autoconfigure.properties.DynamicDataSourceProperties;
import com.tomato.base.dynamic.db.strategy.AbstractDynamicDataSourceStrategy;

/**
 * 指定数据源
 *
 * @author lizhifu
 * @date 2022/3/13
 */
public class SpecialDataSourceStrategy extends AbstractDynamicDataSourceStrategy<String> {
    public SpecialDataSourceStrategy(DynamicDataSourceProperties dynamicDataSourceProperties) {
        super(dynamicDataSourceProperties);
    }

    @Override
    public void dynamicDataSource(String dataSource) {
        DynamicDataSourceContextHolder.setDataSource(dataSource);
    }
}