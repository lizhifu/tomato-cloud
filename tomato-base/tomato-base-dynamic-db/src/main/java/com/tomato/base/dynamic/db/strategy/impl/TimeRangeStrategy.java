package com.tomato.base.dynamic.db.strategy.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * range 方式
 * 每个库一段连续的数据，这个一般是按比如时间范围来的，但是这种一般较少用，
 * 因为很容易产生热点问题，大量的流量都打在最新的数据上了。
 * @author lizhifu
 * @date 2022/3/6
 */
public class TimeRangeStrategy {
    private Logger logger = LoggerFactory.getLogger(TimeRangeStrategy.class);
    
}
