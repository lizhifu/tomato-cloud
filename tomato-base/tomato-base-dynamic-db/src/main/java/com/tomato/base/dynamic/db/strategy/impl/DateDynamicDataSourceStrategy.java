package com.tomato.base.dynamic.db.strategy.impl;

import com.tomato.base.dynamic.db.holder.DynamicDataSourceContextHolder;
import com.tomato.base.dynamic.db.spring.boot.autoconfigure.properties.DynamicDataSourceProperties;
import com.tomato.base.dynamic.db.strategy.AbstractDynamicDataSourceStrategy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 基于时间的分表
 *
 * @author lizhifu
 * @date 2021/12/1
 */
public class DateDynamicDataSourceStrategy extends AbstractDynamicDataSourceStrategy<LocalDateTime> {
    /**
     * 时间格式转换
     */
    private static final DateTimeFormatter YYYYMMDD = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static final DateTimeFormatter YYYYMM = DateTimeFormatter.ofPattern("yyyyMM");
    private static final DateTimeFormatter YYYY = DateTimeFormatter.ofPattern("yyyy");

    public DateDynamicDataSourceStrategy(DynamicDataSourceProperties dynamicDataSourceProperties) {
        super(dynamicDataSourceProperties);
    }

    @Override
    public void dynamicDataSource(LocalDateTime localDateTime) {
        // 年 DynamicTableContextHolder.setTable(localDateTime.format(YYYY));
        // 月 DynamicTableContextHolder.setTable(localDateTime.format(YYYYMM));
        // 日
        DynamicDataSourceContextHolder.setDataSource(localDateTime.format(YYYYMMDD));
    }
}
