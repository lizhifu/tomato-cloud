package com.tomato.base.dynamic.db.strategy;

/**
 * 动态数据源策略
 *
 * @author lizhifu
 * @date 2021/11/30
 */
public interface DynamicDataSourceStrategy<T> extends BaseStrategy<T>{

}
