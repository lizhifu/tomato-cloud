package com.tomato.base.dynamic.db.annotation;


import java.lang.annotation.*;

/**
 * 动态数据源注解
 *
 * @author lizhifu
 * @date 2021/9/13
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DynamicTableAnnotation {
    /** 表名 */
    String table();

    /** 分表字段 */
    String key();

    /**
     * 分库策略，默认取模
     * @return
     */
    String strategy() default "MoldTableStrategy";
}
