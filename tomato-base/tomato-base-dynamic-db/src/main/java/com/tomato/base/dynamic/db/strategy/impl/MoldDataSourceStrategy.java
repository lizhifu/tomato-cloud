package com.tomato.base.dynamic.db.strategy.impl;

import com.tomato.base.dynamic.db.holder.DynamicDataSourceContextHolder;
import com.tomato.base.dynamic.db.spring.boot.autoconfigure.properties.DynamicDataSourceProperties;
import com.tomato.base.dynamic.db.strategy.AbstractDynamicDataSourceStrategy;
import lombok.extern.slf4j.Slf4j;

/**
 * 取模算法
 *
 * @author lizhifu
 * @date 2022/4/6
 */
@Slf4j
public class MoldDataSourceStrategy extends AbstractDynamicDataSourceStrategy<String> {
    public MoldDataSourceStrategy(DynamicDataSourceProperties dynamicDataSourceProperties) {
        super(dynamicDataSourceProperties);
    }

    @Override
    public void dynamicDataSource(String s) {
        int dbKey = Integer.valueOf(s) % super.dynamicDataSourceProperties.getDbCount();
        log.info("MoldDataSourceStrategy 数据库路由 dbKey：{} ", dbKey);
        DynamicDataSourceContextHolder.setDataSource(super.dynamicDataSourceProperties.getPrefix() + dbKey);
    }
}
