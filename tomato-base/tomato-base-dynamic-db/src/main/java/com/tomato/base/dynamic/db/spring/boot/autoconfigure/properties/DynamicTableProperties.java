package com.tomato.base.dynamic.db.spring.boot.autoconfigure.properties;

import lombok.Getter;
import lombok.Setter;

/**
 * 分表配置
 *
 * @author lizhifu
 * @date 2022/4/7
 */
@Getter
@Setter
public class DynamicTableProperties {
    /**
     * 分表数量
     */
    private Integer count;
    /**
     * 分表配置后缀
     */
    private String suffix = "_";
}
