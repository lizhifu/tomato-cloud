package com.tomato.base.redis.redisson.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 测试
 *
 * @author lizhifu
 * @date 2022/3/2
 */
@SpringBootApplication
public class TomatoRedisExampleApplication {

    public static void main(String[] args){
        SpringApplication.run(TomatoRedisExampleApplication.class, args);
        System.out.println("TomatoRedisExampleApplication 启动成功");
    }
}
