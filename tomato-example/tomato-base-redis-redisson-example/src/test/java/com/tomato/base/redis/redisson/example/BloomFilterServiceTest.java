package com.tomato.base.redis.redisson.example;

import com.tomato.base.redis.redisson.bloom.BloomFilterService;
import org.junit.jupiter.api.Test;
import org.redisson.api.RBloomFilter;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * BloomFilterService
 *
 * @author lizhifu
 * @date 2022/4/13
 */
@SpringBootTest
public class BloomFilterServiceTest {
    @Resource
    private BloomFilterService bloomFilterService;
    @Test
    public void test() {
        // 预期插入数量
        long expectedInsertions = 10000L;
        // 错误比率
        double falseProbability = 0.01;
        // 初始化布隆过滤器
        RBloomFilter<Long> bloomFilter = bloomFilterService.create("ipBlackList", expectedInsertions, falseProbability);
        // 布隆过滤器增加元素
        for (long i = 0; i < expectedInsertions; i++) {
            bloomFilter.add(i);
        }
        System.out.println("布隆过滤器增加元素完成:"+bloomFilter.count());
        // 统计误判次数 判断数据 > expectedInsertions 所以是一定不存在于布隆过滤器中的
        int count = 0;
        for (long i = expectedInsertions; i < expectedInsertions * 2; i++) {
            // 布隆过滤器判断元素是否存在
            if (bloomFilter.contains(i)) {
                count++;
            }
        }
        System.out.println("误判次数:"+count);
        // 删除布隆过滤器
        bloomFilter.delete();
    }
}
