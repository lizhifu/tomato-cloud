package com.tomato.study.spring.exception;

import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * spring boot 启动程序
 *
 * @author lizhifu
 * @date 2022/4/22
 */
@SpringBootApplication
public class TomatoStudySpringExceptionApplication {

}
