package com.tomato.study.prometheus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * TomatoStudyPrometheusApplication
 *
 * @author lizhifu
 * @date 2022/4/13
 */
@SpringBootApplication
public class TomatoStudyPrometheusApplication {
    public static void main(String[] args){
        SpringApplication.run(TomatoStudyPrometheusApplication.class, args);
        System.out.println("TomatoStudyPrometheusApplication 服务启动成功");
    }
}
