package com.tomato.study.prometheus.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * DemoController
 *
 * @author lizhifu
 * @date 2022/4/13
 */
@RestController
public class DemoController {
    @GetMapping("/hello")
    public String hello(){
        return "hello";
    }
}
