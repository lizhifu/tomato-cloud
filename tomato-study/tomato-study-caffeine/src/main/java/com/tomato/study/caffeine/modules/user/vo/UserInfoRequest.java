package com.tomato.study.caffeine.modules.user.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 用户信息请求
 *
 * @author lizhifu
 * @date 2022/5/6
 */
@Getter
@Setter
@ToString
public class UserInfoRequest {
    private Integer id;
    private String name;
}
