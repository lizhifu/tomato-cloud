package com.tomato.study.caffeine.modules.user.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 用户信息响应
 *
 * @author lizhifu
 * @date 2022/5/6
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
public class UserInfoResponse {
    private Integer id;
    private String name;
}
