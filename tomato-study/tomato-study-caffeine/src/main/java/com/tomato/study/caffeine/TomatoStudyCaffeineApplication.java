package com.tomato.study.caffeine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * 启动
 *
 * @author lizhifu
 * @date 2022/3/21
 */
@SpringBootApplication
@EnableCaching
public class TomatoStudyCaffeineApplication {
    public static void main(String[] args){
        SpringApplication.run(TomatoStudyCaffeineApplication.class, args);
        System.out.println("服务启动成功");
    }
}
