package com.tomato.study.caffeine.modules.user.controller;

import com.tomato.study.caffeine.modules.user.entity.UserInfo;
import com.tomato.study.caffeine.modules.user.service.UserInfoService;
import com.tomato.study.caffeine.modules.user.vo.UserInfoRequest;
import com.tomato.study.caffeine.modules.user.vo.UserInfoResponse;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * 用户信息控制器
 *
 * @author lizhifu
 * @date 2022/5/6
 */
@RestController
public class UserInfoController {
    @Resource
    private UserInfoService userInfoService;

    @GetMapping("/userInfo/{id}")
    public Object getUserInfo(@PathVariable Integer id) {
        UserInfo userInfo = userInfoService.getById(id);
        UserInfoResponse userInfoResponse =
                Optional.ofNullable(userInfo).map(u->new UserInfoResponse().setId(u.getId()).setName(u.getName())).orElse(null);
        return userInfoResponse;
    }

    @PostMapping("/userInfo")
    public Object createUserInfo(@RequestBody UserInfoRequest userInfoRequest) {
        userInfoService.addUserInfo(userInfoRequest);
        return "SUCCESS";
    }

    @DeleteMapping("/userInfo/{id}")
    public Object deleteUserInfo(@PathVariable Integer id) {
        userInfoService.deleteById(id);
        return "SUCCESS";
    }
}
