package com.tomato.study.caffeine.config;

import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.text.MessageFormat;

/**
 * 自定义key生成器
 * 使用方法： @Cacheable(value = "vv", keyGenerator = "customKeyGenerator")
 *
 * @author lizhifu
 * @date 2022/5/6
 */
@Component("customKeyGenerator")
public class CustomKeyGenerator extends SimpleKeyGenerator {
    @Override
    public Object generate(Object target, Method method, Object... params) {
        Object generate = super.generate(target, method, params);
        return MessageFormat.format("{0}{1}{2}", method.toGenericString(), generate);
    }
}
