package com.tomato.study.caffeine.modules.user.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 用户信息
 *
 * @author lizhifu
 * @date 2022/5/6
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
public class UserInfo {
    private Integer id;
    private String name;
}
