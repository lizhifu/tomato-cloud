package com.tomato.study.caffeine.modules.user.service;

import com.tomato.study.caffeine.modules.user.entity.UserInfo;
import com.tomato.study.caffeine.modules.user.vo.UserInfoRequest;
import com.tomato.study.caffeine.modules.user.vo.UserInfoResponse;

/**
 * 用户信息服务接口
 *
 * @author lizhifu
 * @date 2022/5/6
 */
public interface UserInfoService {
    /**
     * 增加用户信息
     *
     * @param userInfoRequest 用户信息
     * @return 用户信息
     */
    UserInfo addUserInfo(UserInfoRequest userInfoRequest);

    /**
     * 获取用户信息
     *
     * @param id 用户ID
     * @return 用户信息
     */
    UserInfo getById(Integer id);

    /**
     * 删除用户信息
     *
     * @param id 用户ID
     */
    void deleteById(Integer id);
}
