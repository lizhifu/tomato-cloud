package com.tomato.study.caffeine.modules.user.mapper;

import com.tomato.study.caffeine.modules.user.entity.UserInfo;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * 模拟 mapper
 *
 * @author lizhifu
 * @date 2022/5/6
 */
@Service
public class UserInfoMapper {
    /**
     * 模拟数据库存储数据
     */
    private HashMap<Integer, UserInfo> userInfoMap = new HashMap<>();
    public void insert(UserInfo userInfo) {
        userInfoMap.put(userInfo.getId(), userInfo);
    }

    public UserInfo selectById(Integer id) {
        return userInfoMap.get(id);
    }

    public void deleteById(Integer id) {
        userInfoMap.remove(id);
    }
}
