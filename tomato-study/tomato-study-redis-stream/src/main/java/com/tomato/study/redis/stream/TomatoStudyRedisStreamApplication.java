package com.tomato.study.redis.stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * redis stream 实例
 *
 * @author lizhifu
 * @date 2022/5/11
 */
@SpringBootApplication
public class TomatoStudyRedisStreamApplication {
    public static void main(String[] args){
        SpringApplication.run(TomatoStudyRedisStreamApplication.class, args);
        System.out.println("服务启动成功");
    }
}
