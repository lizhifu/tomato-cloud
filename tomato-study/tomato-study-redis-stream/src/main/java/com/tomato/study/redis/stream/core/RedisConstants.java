package com.tomato.study.redis.stream.core;

/**
 * redis常量
 *
 * @author lizhifu
 * @date 2022/5/11
 */
public class RedisConstants {
    /**
     * redis stream 唯一标识
     */
    public static final String REDIS_STREAM1 = "stream1";
    /**
     * group1
     *  --> consumer1
     *  --> consumer2
     */
    public static final String REDIS_STREAM1_GROUP1 = "group1";
    public static final String REDIS_STREAM1_GROUP1_CONSUMER1 = "consumer1";
    public static final String REDIS_STREAM1_GROUP1_CONSUMER2 = "consumer2";

    /**
     * group2 --> consumer1
     */
    public static final String REDIS_STREAM1_GROUP2 = "group2";
    public static final String REDIS_STREAM1_GROUP2_CONSUMER1 = "consumer1";

}
