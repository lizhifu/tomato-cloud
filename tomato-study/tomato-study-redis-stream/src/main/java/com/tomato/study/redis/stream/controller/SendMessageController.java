package com.tomato.study.redis.stream.controller;

import com.tomato.study.redis.stream.core.RedisConstants;
import com.tomato.study.redis.stream.service.RedisStreamService;
import org.springframework.data.redis.connection.stream.PendingMessagesSummary;
import org.springframework.data.redis.connection.stream.StreamInfo;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * 发送消息控制器
 *
 * @author lizhifu
 * @date 2022/5/11
 */
@RestController
public class SendMessageController {
    @Resource
    private RedisStreamService redisStreamService;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @GetMapping("/sendMessage")
    public String sendMessage() {
        // 发送消息
        String add = redisStreamService.add(RedisConstants.REDIS_STREAM1, UUID.randomUUID().toString(), "停止精神内耗");
        return add;
    }

    @GetMapping("/group")
    public StreamInfo.XInfoConsumers group() {
        // 创建消费组:stream1
        StreamInfo.XInfoConsumers string = redisStreamService.consumers(RedisConstants.REDIS_STREAM1, RedisConstants.REDIS_STREAM1_GROUP1);
        return string;
    }
    @GetMapping("/pending")
    public PendingMessagesSummary pending() {
        PendingMessagesSummary pending = stringRedisTemplate.opsForStream().pending(RedisConstants.REDIS_STREAM1, RedisConstants.REDIS_STREAM1_GROUP2);
        return pending;
    }
}
