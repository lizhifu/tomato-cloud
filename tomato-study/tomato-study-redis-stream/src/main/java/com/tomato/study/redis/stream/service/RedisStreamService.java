package com.tomato.study.redis.stream.service;

import org.springframework.data.redis.connection.stream.MapRecord;
import org.springframework.data.redis.connection.stream.RecordId;
import org.springframework.data.redis.connection.stream.StreamInfo;
import org.springframework.data.redis.connection.stream.StreamOffset;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * redis stream 命令
 *
 * @author lizhifu
 * @date 2022/5/11
 */
@Component
public class RedisStreamService {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 创建消费组
     * @param streamKey stream 唯一标识
     * @param consumerGroupKey 消费组唯一标识
     * @return
     */
    public String createGroup(String streamKey, String consumerGroupKey){
        return stringRedisTemplate.opsForStream().createGroup(streamKey, consumerGroupKey);
    }

    /**
     * 消费组信息
     * @param streamKey stream 唯一标识
     * @param consumerGroupKey 消费组唯一标识
     * @return
     */
    public StreamInfo.XInfoConsumers consumers(String streamKey, String consumerGroupKey){
        return stringRedisTemplate.opsForStream().consumers(streamKey, consumerGroupKey);
    }

    /**
     * 确认已消费
     * @param key
     * @param group
     * @param recordIds
     * @return
     */
    public Long ack(String key, String group, String... recordIds){
        return stringRedisTemplate.opsForStream().acknowledge(key, group, recordIds);
    }

    /**
     * 追加消息
     * @param streamKey stream 唯一标识
     * @param field
     * @param value
     * @return
     */
    public String add(String streamKey, String field, Object value){
        Map<String, Object> content = new HashMap<>(1);
        content.put(field, value);
        return add(streamKey, content);
    }

    public String add(String streamKey, Map<String, Object> content){
        return stringRedisTemplate.opsForStream().add(streamKey, content).getValue();
    }

    /**
     * 删除消息，这里的删除仅仅是设置了标志位，不影响消息总长度
     * 消息存储在stream的节点下，删除时仅对消息做删除标记，当一个节点下的所有条目都被标记为删除时，销毁节点
     * @param key
     * @param recordIds
     * @return
     */
    public Long del(String key, String... recordIds){
        return stringRedisTemplate.opsForStream().delete(key, recordIds);
    }

    /**
     * 消息长度
     * @param key
     * @return
     */
    public Long len(String key){
        return stringRedisTemplate.opsForStream().size(key);
    }

    /**
     * 从开始读
     * @param key
     * @return
     */
    public List<MapRecord<String, Object, Object>> read(String key){
        return stringRedisTemplate.opsForStream().read(StreamOffset.fromStart(key));
    }

    /**
     * 从指定的ID开始读
     * @param key
     * @param recordId
     * @return
     */
    public List<MapRecord<String, Object, Object>> read(String key, String recordId){
        return stringRedisTemplate.opsForStream().read(StreamOffset.from(MapRecord.create(key, new HashMap<>(1)).withId(RecordId.of(recordId))));
    }
}
