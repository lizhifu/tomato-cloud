package com.tomato.study.redis.stream.consumer.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.stream.MapRecord;
import org.springframework.data.redis.stream.StreamListener;
import org.springframework.stereotype.Component;

/**
 * 消费者消息监听器
 *
 * @author lizhifu
 * @date 2022/5/11
 */
@Slf4j
@Component
public class RedisStreamListener implements StreamListener<String, MapRecord<String, String, String>> {
    @Override
    public void onMessage(MapRecord<String, String, String> message) {
        log.info("RedisStreamListener 消费者消息监听器：{}", message);
        // 接收到消息
        log.info("RedisStreamListener message id " + message.getId());
        log.info("RedisStreamListener stream " + message.getStream());
        log.info("RedisStreamListener body " + message.getValue());
    }
}
