package com.tomato.study.rabbitmq.consumer.consumer;

import com.tomato.study.rabbitmq.core.constant.RabbitConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消费者监听队列，并进行消费
 *
 * @author lizhifu
 * @date 2022/3/21
 */
@Component
@Slf4j
public class RabbitPublishSubscribeConsumer {
    @RabbitListener(queues = RabbitConstant.PUBLISH_SUBSCRIBE_FIRST_QUEUE_NAME)
    @RabbitHandler
    public void pubsubQueueFirst(String context) {
        log.info("RabbitPublishSubscribeConsumer 第一个队列接收消息：{}",context);
    }

    @RabbitListener(queues = RabbitConstant.PUBLISH_SUBSCRIBE_SECOND_QUEUE_NAME)
    @RabbitHandler
    public void pubsubQueueSecond(String context) {
        log.info("RabbitPublishSubscribeConsumer 第二个队列接收消息：{}",context);
    }
}
