package com.tomato.study.rabbitmq.consumer.consumer;

import com.tomato.study.rabbitmq.core.constant.RabbitConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消费者监听队列，并消费消息（这里有两个消费者监听同一队列）
 * 响应结果（由于有两个消费者监听同一队列，消息只能被其中一者进行消费，默认是负载均衡的将消息发送给所有消费者）
 * @author lizhifu
 * @date 2022/3/21
 */
@Component
@Slf4j
public class RabbitWorkConsumer {
    @RabbitListener(queues = RabbitConstant.WORK_QUEUE_NAME)
    @RabbitHandler
    public void workQueueListenerFirst(String context) {
        log.info("RabbitWorkConsumer 第一个接收消息：{}",context);
    }

    @RabbitListener(queues = RabbitConstant.WORK_QUEUE_NAME)
    @RabbitHandler
    public void workQueueListenerSecond(String context) {
        log.info("RabbitWorkConsumer 第二个接收消息：{}",context);
    }
}
