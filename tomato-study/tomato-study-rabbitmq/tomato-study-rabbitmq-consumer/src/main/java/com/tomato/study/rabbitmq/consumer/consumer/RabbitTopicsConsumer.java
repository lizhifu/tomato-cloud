package com.tomato.study.rabbitmq.consumer.consumer;

import com.tomato.study.rabbitmq.core.constant.RabbitConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消费者监听队列，并进行消费
 *
 * @author lizhifu
 * @date 2022/3/21
 */
@Component
@Slf4j
public class RabbitTopicsConsumer {
    @RabbitListener(queues = RabbitConstant.TOPICS_FIRST_QUEUE_NAME)
    @RabbitHandler
    public void topicFirstQueue(String context) {
        log.info("RabbitTopicsConsumer topicFirstQueue 接收消息：{}",context);
    }

    @RabbitListener(queues = RabbitConstant.TOPICS_SECOND_QUEUE_NAME)
    @RabbitHandler
    public void topicSecondQueue(String context) {
        log.info("RabbitTopicsConsumer topicSecondQueue 接收消息：{}",context);
    }

    @RabbitListener(queues = RabbitConstant.TOPICS_THIRD_QUEUE_NAME)
    @RabbitHandler
    public void topicThirdQueue(String context) {
        log.info("RabbitTopicsConsumer topicThirdQueue 接收消息：{}",context);
    }
}
