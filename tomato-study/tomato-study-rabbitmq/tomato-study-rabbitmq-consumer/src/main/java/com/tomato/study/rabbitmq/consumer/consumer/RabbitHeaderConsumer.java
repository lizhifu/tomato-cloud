package com.tomato.study.rabbitmq.consumer.consumer;

import com.tomato.study.rabbitmq.core.constant.RabbitConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消费者监听队列，并进行消费
 *
 * @author lizhifu
 * @date 2022/3/21
 */
@Component
@Slf4j
public class RabbitHeaderConsumer {
    @RabbitListener(queues = RabbitConstant.HEADER_FIRST_QUEUE_NAME)
    @RabbitHandler
    public void headerFirstQueue(String context) {
        log.info("RabbitPublishSubscribeConsumer headerFirstQueue 接收消息：{}",context);
    }

    @RabbitListener(queues = RabbitConstant.HEADER_SECOND_QUEUE_NAME)
    @RabbitHandler
    public void headerSecondQueue(String context) {
        log.info("RabbitPublishSubscribeConsumer headerSecondQueue 接收消息：{}",context);
    }
}
