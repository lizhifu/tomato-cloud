package com.tomato.study.rabbitmq.consumer.consumer;

import com.tomato.study.rabbitmq.core.constant.RabbitConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消费者监听队列，并进行消费
 *
 * @author lizhifu
 * @date 2022/3/21
 */
@Component
@Slf4j
public class RabbitRoutingConsumer {
    @RabbitListener(queues = RabbitConstant.ROUTING_FIRST_QUEUE_NAME)
    @RabbitHandler
    public void routingFirstQueueListener(String context) {
        log.info("RabbitRoutingConsumer 第一队列接收消息：{}",context);
    }

    @RabbitListener(queues = RabbitConstant.ROUTING_SECOND_QUEUE_NAME)
    @RabbitHandler
    public void routingSecondQueueListener(String context) {
        log.info("RabbitRoutingConsumer 第二队列接收消息：{}",context);
    }

    @RabbitListener(queues = RabbitConstant.ROUTING_THIRD_QUEUE_NAME)
    @RabbitHandler
    public void routingThirdQueueListener(String context) {
        log.info("RabbitRoutingConsumer 第三队列接收消息：{}",context);

    }
}
