package com.tomato.study.rabbitmq.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 消费者
 *
 * @author lizhifu
 * @date 2022/3/21
 */
@SpringBootApplication
public class TomatoStudyRabbitmqConsumerApplication {
    public static void main(String[] args){
        SpringApplication.run(TomatoStudyRabbitmqConsumerApplication.class, args);
        System.out.println("消费者 服务启动成功");
    }
}
