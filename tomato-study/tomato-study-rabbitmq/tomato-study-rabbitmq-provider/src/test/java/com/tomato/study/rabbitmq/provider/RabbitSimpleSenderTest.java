package com.tomato.study.rabbitmq.provider;

import com.tomato.study.rabbitmq.provider.service.RabbitSimpleSender;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * RabbitSimpleSenderTest
 *
 * @author lizhifu
 * @date 2022/3/21
 */
@SpringBootTest
public class RabbitSimpleSenderTest {
    @Resource
    RabbitSimpleSender rabbitSimpleSender;
    @Test
    public void simpleTransactionQueue(){
        rabbitSimpleSender.sendSimpleTransactionQueue(1);
    }
    @Test
    public void sendSimpleConfirmQueue(){
        rabbitSimpleSender.sendSimpleConfirmQueue("confirm 确认机制");
    }
}
