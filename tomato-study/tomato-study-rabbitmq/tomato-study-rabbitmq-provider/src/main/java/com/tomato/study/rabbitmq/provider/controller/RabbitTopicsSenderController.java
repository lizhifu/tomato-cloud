package com.tomato.study.rabbitmq.provider.controller;

import com.tomato.study.rabbitmq.core.constant.RabbitConstant;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Locale;
import java.util.UUID;

/**
 * 主题模式
 *
 * @author lizhifu
 * @date 2022/3/21
 */
@RestController
public class RabbitTopicsSenderController {
    @Resource
    private RabbitTemplate rabbitTemplate;
    @GetMapping("/topics")
    public String topics(){
        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString().toUpperCase(Locale.ROOT));
        // 给第一个队列发送消息，此时队列能接受到消息，因为队列通配符为 #.first.#，而 routing_key 为 topics.first.routing.key，匹配成功
        rabbitTemplate.convertAndSend(RabbitConstant.TOPICS_EXCHANGE_NAME, RabbitConstant.TOPICS_FIRST_QUEUE_ROUTING_KEY, "topics hello",correlationData);
        // 给第二个队列发送消息，此时队列也能接受到消息，因为队列通配符为 *.second.#，而 routing_key 为 topics.second.routing.key，匹配成功
        rabbitTemplate.convertAndSend(RabbitConstant.TOPICS_EXCHANGE_NAME, RabbitConstant.TOPICS_SECOND_QUEUE_ROUTING_KEY, "topics hello",correlationData);
        // 给第三个队列发送消息，此时队列无法接受到消息，因为队列通配符为 *.third.*，而 routing_key 为 topics.third.routing.key，匹配失败
        rabbitTemplate.convertAndSend(RabbitConstant.TOPICS_EXCHANGE_NAME, RabbitConstant.TOPICS_THIRD_QUEUE_ROUTING_KEY, "topics hello",correlationData);
        return "hello";
    }
}
