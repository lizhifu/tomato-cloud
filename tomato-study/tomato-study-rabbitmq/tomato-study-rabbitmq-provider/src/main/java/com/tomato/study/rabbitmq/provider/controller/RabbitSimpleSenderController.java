package com.tomato.study.rabbitmq.provider.controller;

import com.tomato.study.rabbitmq.core.constant.RabbitConstant;
import com.tomato.study.rabbitmq.provider.service.RabbitSimpleSender;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Locale;
import java.util.UUID;

/**
 * RabbitSimpleSender
 *
 * @author lizhifu
 * @date 2022/3/21
 */
@RestController
public class RabbitSimpleSenderController {
    @Resource
    private RabbitSimpleSender rabbitSimpleSender;
    @Resource
    private RabbitTemplate rabbitTemplate;
    @GetMapping("/send")
    public String send(){
        rabbitSimpleSender.sendSimpleConfirmQueue("confirm 确认机制");
        return "hello";
    }
    @GetMapping("/send/simple")
    public String sendSimple(){
        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString().toUpperCase(Locale.ROOT));
        rabbitTemplate.convertAndSend(RabbitConstant.SIMPLE_QUEUE_NAME, (Object) "hello world!",correlationData);
        return "hello";
    }
}