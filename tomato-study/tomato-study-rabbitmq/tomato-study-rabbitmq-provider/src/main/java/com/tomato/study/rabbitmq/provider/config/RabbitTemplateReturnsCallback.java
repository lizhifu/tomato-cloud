package com.tomato.study.rabbitmq.provider.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * 发生异常时的消息返回提醒
 * Exchange -> Queue
 * @author lizhifu
 * @date 2022/3/21
 */
@Slf4j
@Component
public class RabbitTemplateReturnsCallback implements RabbitTemplate.ReturnsCallback {
    @Resource
    private RabbitTemplate rabbitTemplate;
    @PostConstruct
    public void init(){
        //指定 ConfirmCallback
        rabbitTemplate.setMandatory(true);
        rabbitTemplate.setReturnsCallback(this);
    }
    /**
     * ReturnCallback消息没有正确到达队列时触发回调，如果正确到达队列不执行
     * config : 需要开启rabbitmq发送失败回退
     * @param returned
     */
    @Override
    public void returnedMessage(ReturnedMessage returned) {
        log.error("匹配queue失败:{}", returned.toString());
    }
}
