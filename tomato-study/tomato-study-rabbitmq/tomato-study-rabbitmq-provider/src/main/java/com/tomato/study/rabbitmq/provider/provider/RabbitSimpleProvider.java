package com.tomato.study.rabbitmq.provider.provider;

import com.tomato.study.rabbitmq.core.constant.RabbitConstant;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * 生产者声明队列，并向队列发送消息
 *
 * @author lizhifu
 * @date 2022/3/21
 */
@Configuration
public class RabbitSimpleProvider {
    @Bean
    public Queue simpleQueue() {
        return new Queue(RabbitConstant.SIMPLE_QUEUE_NAME);
    }

    /**
     * 使用事务
     * @return
     */
    @Bean
    public Queue simpleTransactionQueue() {
        return new Queue(RabbitConstant.SIMPLE_QUEUE_TRANSACTION_NAME);
    }

    /**
     * 持久化--默认就是 durable
     * @return
     */
    @Bean
    public Queue simplePersistenceQueue() {
        return new Queue(RabbitConstant.SIMPLE_QUEUE_PERSISTENCE_NAME,true);
    }
}