package com.tomato.study.rabbitmq.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 生产者
 *
 * @author lizhifu
 * @date 2022/3/21
 */
@SpringBootApplication
public class TomatoStudyRabbitmqProviderApplication {
    public static void main(String[] args){
        SpringApplication.run(TomatoStudyRabbitmqProviderApplication.class, args);
        System.out.println("生产者 服务启动成功");
    }
}
