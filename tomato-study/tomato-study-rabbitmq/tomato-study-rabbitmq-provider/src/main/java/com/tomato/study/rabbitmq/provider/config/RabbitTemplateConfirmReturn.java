package com.tomato.study.rabbitmq.provider.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * 消息发送成功的回调
 * 需要开启发送确认
 * 生产者 ==>> Exchange 的回调确认
 * publisher-confirm-type 配置为 CORRELATED 或者 SIMPLE 时都会在此处进行回调。
 * @author lizhifu
 * @date 2022/3/21
 */
@Slf4j
@Component
public class RabbitTemplateConfirmReturn implements RabbitTemplate.ConfirmCallback {
    @Resource
    private RabbitTemplate rabbitTemplate;

    @PostConstruct
    public void init(){
        //指定 ConfirmCallback
        rabbitTemplate.setConfirmCallback(this);
    }
    /**
     *
     * @param correlationData 回调的相关数据
     * @param ack ack为真，nack为假
     * @param cause 一个可选的原因，用于nack，如果可用，否则为空
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        log.info("消息唯一标识："+correlationData);
        log.info("确认结果："+ack);
        log.info("失败原因："+cause);
    }
}
