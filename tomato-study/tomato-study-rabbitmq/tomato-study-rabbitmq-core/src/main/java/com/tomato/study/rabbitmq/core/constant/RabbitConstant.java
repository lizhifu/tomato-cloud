package com.tomato.study.rabbitmq.core.constant;

/**
 * RabbitConstant
 *
 * @author lizhifu
 * @date 2022/3/21
 */
public class RabbitConstant {
    /**
     * 简单模式
     */
    public static final String SIMPLE_QUEUE_NAME = "simple_queue";
    /**
     * 简单模式-事务
     */
    public static final String SIMPLE_QUEUE_TRANSACTION_NAME = "simple_queue_transaction";
    /**
     * 简单模式-持久化(persistence)
     */
    public static final String SIMPLE_QUEUE_PERSISTENCE_NAME = "simple_queue_persistence";



    /**
     * work 模式
     */
    public static final String WORK_QUEUE_NAME = "work_queue";

    /**
     * 发布/订阅（publish/subscribe）模式
     */
    public static final String PUBLISH_SUBSCRIBE_EXCHANGE_NAME = "publish_subscribe_exchange";

    public static final String PUBLISH_SUBSCRIBE_FIRST_QUEUE_NAME = "publish_subscribe_first_queue";
    public static final String PUBLISH_SUBSCRIBE_SECOND_QUEUE_NAME = "publish_subscribe_second_queue";

    /**
     * 路由（routing）模式
     */
    public static final String ROUTING_EXCHANGE_NAME = "routing_exchange";
    public static final String ROUTING_FIRST_QUEUE_NAME = "routing_first_queue";
    public static final String ROUTING_SECOND_QUEUE_NAME = "routing_second_queue";
    public static final String ROUTING_THIRD_QUEUE_NAME = "routing_third_queue";

    public static final String ROUTING_FIRST_QUEUE_ROUTING_KEY_NAME = "routing_first_queue_routing_key";
    public static final String ROUTING_SECOND_QUEUE_ROUTING_KEY_NAME = "routing_second_queue_routing_key";
    public static final String ROUTING_THIRD_QUEUE_ROUTING_KEY_NAME = "routing_third_queue_routing_key";

    /**
     * 主题（topics）模式
     */
    public static final String TOPICS_EXCHANGE_NAME = "topics_exchange";
    // 三个队列
    public static final String TOPICS_FIRST_QUEUE_NAME = "topics_first_queue";
    public static final String TOPICS_SECOND_QUEUE_NAME = "topics_second_queue";
    public static final String TOPICS_THIRD_QUEUE_NAME = "topics_third_queue";
    // 三个 routing key
    public static final String TOPICS_FIRST_QUEUE_ROUTING_KEY = "topics.first.routing.key";
    public static final String TOPICS_SECOND_QUEUE_ROUTING_KEY = "topics.second.routing.key";
    public static final String TOPICS_THIRD_QUEUE_ROUTING_KEY = "topics.third.routing.key";
    // 队列（queue） 和 交换机（Exchange） 通过 routing key 进行绑定
    public static final String TOPICS_ROUTING_KEY_FIRST_WILDCARD = "#.first.#";
    public static final String TOPICS_ROUTING_KEY_SECOND_WILDCARD = "*.second.#";
    public static final String TOPICS_ROUTING_KEY_THIRD_WILDCARD = "*.third.*";

    /**
     * header 模式
     */
    public static final String HEADER_EXCHANGE_NAME = "header_exchange";
    public static final String HEADER_FIRST_QUEUE_NAME = "header_first_queue";
    public static final String HEADER_SECOND_QUEUE_NAME = "header_second_queue";

    /**
     * rpc 模式
     */
    public static final String RPC_QUEUE_NAME = "rpc_queue";
}
