package com.tomato.study.mybatis.script;

import com.tomato.study.mybatis.script.domain.Demo;
import com.tomato.study.mybatis.script.mapper.DemoMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * DemoMapper
 *
 * @author lizhifu
 * @date 2022/5/19
 */
@SpringBootTest
public class DemoMapperTest {
    @Resource
    DemoMapper demoMapper;

    @Test
    public void test() {
        Demo demo = new Demo();
        demo.setA1("a1");
        demoMapper.insert(demo);
        System.out.println(demo);
    }
}
