package com.tomato.study.mybatis.script;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * mybatis-script
 *
 * @author lizhifu
 * @date 2022/3/21
 */
@SpringBootApplication
public class TomatoStudyMybatisApplication {
    public static void main(String[] args){
        SpringApplication.run(TomatoStudyMybatisApplication.class, args);
        System.out.println("mybatis-script 服务启动成功");
    }
}
