package com.tomato.study.mybatis.script.mapper;

import com.tomato.study.mybatis.script.domain.Demo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;

/**
 * DemoMapper
 *
 * @author lizhifu
 * @date 2022/5/19
 */
@Mapper
public interface DemoMapper {
    @Insert("DemoMapper/insert.ftl")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(@Param("demo") Demo demo);
}
