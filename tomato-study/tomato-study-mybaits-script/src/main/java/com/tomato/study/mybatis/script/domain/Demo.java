package com.tomato.study.mybatis.script.domain;

import lombok.Data;

/**
 * Demo
 *
 * @author lizhifu
 * @date 2022/5/19
 */
@Data
public class Demo {
    private Long id;
    private String a1;
}
