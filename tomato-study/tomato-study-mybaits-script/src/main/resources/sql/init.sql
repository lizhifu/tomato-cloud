-- auto-generated definition
drop table if exists `demo`;
create table demo
(
    id bigint auto_increment comment 'id' primary key,
    a1 varchar(255) not null default '测试' comment 'a1',
    a2 varchar(255)  comment 'a2'
)comment 'demo';
