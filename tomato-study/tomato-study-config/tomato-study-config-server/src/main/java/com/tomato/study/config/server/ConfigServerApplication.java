package com.tomato.study.config.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * 启动类
 *
 * @author lizhifu
 * @date 2022/4/29
 */
@SpringBootApplication
@EnableConfigServer  // 开启 Spring Cloud Config 配置中心功能
public class ConfigServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConfigServerApplication.class, args);
    }
}
