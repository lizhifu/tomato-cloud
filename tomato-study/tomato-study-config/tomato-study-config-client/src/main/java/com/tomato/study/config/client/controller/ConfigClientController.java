package com.tomato.study.config.client.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 获取配置信息
 *
 * @author lizhifu
 * @date 2022/4/29
 */
@RestController
public class ConfigClientController {
    @Value("${server.version}")
    private String version;
    @Value("${server.ip}")
    private String ip;

    @GetMapping(value = "/getServer")
    public String getServer() {
        return "version：" + version + "ip：" + ip;
    }
}
