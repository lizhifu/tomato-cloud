package com.tomato.study.spring.service;

/**
 * SpringDemoService
 *
 * @author lizhifu
 * @date 2022/3/5
 */
public interface SpringDemoService {
    /**
     * demo
     */
    public void demo();
}
