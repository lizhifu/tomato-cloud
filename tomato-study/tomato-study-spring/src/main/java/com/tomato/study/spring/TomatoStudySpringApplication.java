package com.tomato.study.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * spring study
 *
 * @author lizhifu
 * @date 2022/3/5
 */
@SpringBootApplication
public class TomatoStudySpringApplication {
    public static void main(String[] args){
        SpringApplication sa = new SpringApplication(TomatoStudySpringApplication.class);
        //spring:
        //  main:
        //    allow-circular-references: true
//        sa.setAllowCircularReferences(Boolean.TRUE);
        sa.run(args);
        System.out.println("spring study 服务启动成功");
    }
}
