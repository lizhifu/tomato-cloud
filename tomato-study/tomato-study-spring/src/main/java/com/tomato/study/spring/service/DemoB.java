package com.tomato.study.spring.service;

/**
 * DemoB
 *
 * @author lizhifu
 * @date 2022/4/22
 */
public interface DemoB {
    void sayHello();
}
