package com.tomato.study.spring.service.impl;

import com.tomato.study.spring.service.SpringDemoService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * SpringDemoService
 *
 * @author lizhifu
 * @date 2022/3/5
 */
@Service
public class SpringDemoServiceImpl implements SpringDemoService {
    @Override
    public void demo() {
        System.out.println("demo " + LocalDateTime.now());
    }
}
