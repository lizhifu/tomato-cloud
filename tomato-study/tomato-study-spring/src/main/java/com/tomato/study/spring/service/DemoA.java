package com.tomato.study.spring.service;

/**
 * DemoA
 *
 * @author lizhifu
 * @date 2022/4/22
 */
public interface DemoA {
    void sayHello();
}
