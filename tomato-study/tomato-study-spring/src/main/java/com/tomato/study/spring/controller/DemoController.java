package com.tomato.study.spring.controller;

import com.tomato.study.spring.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * demo controller
 *
 * @author lizhifu
 * @date 2022/4/22
 */
@RestController
public class DemoController {

    @Autowired
    private DemoService demoService;

    @GetMapping("/hello")
    public String hello(String name) {
        return demoService.sayHello(name);
    }

    private final DemoService demoService1;
    @Autowired
    public DemoController(DemoService demoService1) {
        this.demoService1 = demoService1;
    }

    private DemoService demoService2;
    @Autowired // @Autowired 注解是可以不写的
    public void setDemoService(DemoService demoService2) {
        this.demoService2 = demoService2;
    }
}
