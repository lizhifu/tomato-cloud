package com.tomato.study.spring.service.impl;

import com.tomato.study.spring.service.DemoA;
import com.tomato.study.spring.service.DemoB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * DemoAImpl
 *
 * @author lizhifu
 * @date 2022/4/22
 */
public class DemoAImpl implements DemoA {
    @Autowired
    private DemoB demoB;
    @Override
    public void sayHello() {
        System.out.println("DemoA");
    }
}
