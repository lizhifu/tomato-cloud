package com.tomato.study.spring.service.rely;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 循环依赖测试
 *
 * @author lizhifu
 * @date 2022/5/6
 */
//@Service("bRelyService")
//@DependsOn(value = "aRelyService")
public class BRelyService {
    @Autowired
    private ARelyService arelyService;
    public void bRely() {
        System.out.println("bRely");
    }
}
