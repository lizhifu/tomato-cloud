package com.tomato.study.spring.controller;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 请求模拟
 *
 * @author lizhifu
 * @date 2022/5/12
 */
@RestController
public class ShutdownDemoController implements DisposableBean {
    @GetMapping("/shutdownDemo")
    public String shutdownDemo() throws InterruptedException {
        // 业务耗时处理流程
        Thread.sleep(15 * 1000L);
        return "hello";
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("destroy bean.....");
    }
}
