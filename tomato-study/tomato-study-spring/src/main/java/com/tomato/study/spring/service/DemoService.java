package com.tomato.study.spring.service;

/**
 * DemoService
 *
 * @author lizhifu
 * @date 2022/4/22
 */
public interface DemoService {

    String sayHello(String name);
}
