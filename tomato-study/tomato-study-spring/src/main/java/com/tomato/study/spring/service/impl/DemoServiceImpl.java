package com.tomato.study.spring.service.impl;

import com.tomato.study.spring.service.DemoService;
import org.springframework.stereotype.Service;

/**
 * DemoServiceImpl
 *
 * @author lizhifu
 * @date 2022/4/22
 */
@Service
public class DemoServiceImpl implements DemoService {
    @Override
    public String sayHello(String name) {
        return name;
    }
}
