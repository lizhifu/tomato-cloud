package com.tomato.study.spring.service.rely;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 循环依赖测试
 *
 * @author lizhifu
 * @date 2022/5/6
 */
//@Service("aRelyService")
//@DependsOn(value = "bRelyService")
public class ARelyService {
    @Autowired
    private BRelyService bRelyService;
    public void aRely() {
        System.out.println("aRely");
    }
}
