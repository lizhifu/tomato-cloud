package com.tomato.study.spring;

import com.tomato.study.spring.service.SpringDemoService;
import com.tomato.study.spring.service.SpringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * SpringGetServiceTest
 *
 * @author lizhifu
 * @date 2022/3/5
 */
@SpringBootTest
public class SpringGetServiceTest {
    @Test
    public void test(){
        SpringDemoService springDemoService = SpringUtils.getBean("springDemoServiceImpl");
        springDemoService.demo();
    }
}
