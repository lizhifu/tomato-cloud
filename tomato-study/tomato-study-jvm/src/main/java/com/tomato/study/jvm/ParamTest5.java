package com.tomato.study.jvm;

/**
 * ParamTest
 *
 * @author lizhifu
 * @date 2022/3/16
 */
public class ParamTest5 {
    public static void main(String[] args) {
        UserParam userParam = new UserParam();
        userParam.setAge(18);
        userParam.setName("小明");
        System.out.println("前："+userParam.toString());
        change(userParam);
        System.out.println("后："+userParam.toString());
    }
    public static void change(UserParam userParam1){
        userParam1 = new UserParam();
        userParam1.setName("小红");
    }
}
