package com.tomato.study.jvm.lombokdemo;


import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * lombok demo
 *
 * @author lizhifu
 * @date 2022/4/27
 */
@Getter
@Setter
@Accessors(chain = true)
@RequiredArgsConstructor(staticName = "ofName")
public class DemoDto {
    @NonNull
    private String name;
    private int age;
}