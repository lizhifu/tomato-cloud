package com.tomato.study.jvm;

/**
 * ParamTest
 *
 * @author lizhifu
 * @date 2022/3/16
 */
public class ParamTest {
    public static void main(String[] args) {
        ParamTest paramTest = new ParamTest();
        // 你好 为实际参数
        paramTest.setMsg("你好");
    }
    public void setMsg(String msg){
        // msg 为形式参数
        System.out.println(msg);
    }
}
