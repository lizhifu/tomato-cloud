package com.tomato.study.jvm.juc;

/**
 * VolatileTest
 *
 * @author lizhifu
 * @date 2022/3/20
 */
public class VolatileTest2 {
    public volatile boolean shutdownRequested;

    public void shutdown() {
        shutdownRequested = true;
    }

    public void doWork() {
       if(!shutdownRequested){
           System.out.println("关闭了");
       }
    }
    public static void main(String[] args) {

    }
}
