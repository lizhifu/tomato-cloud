package com.tomato.study.jvm;

/**
 * BitTest
 *
 * @author lizhifu
 * @date 2022/4/12
 */
public class BitTest {
    public static void main(String[] args) {
        int i = 29;
        int COUNT_BITS = Integer.SIZE - 3;
        int RUNNING    = -1 << COUNT_BITS;
        int SHUTDOWN   =  0 << COUNT_BITS;
        int STOP       =  1 << COUNT_BITS;
        int TIDYING    =  2 << COUNT_BITS;
        int TERMINATED =  3 << COUNT_BITS;
        int CAPACITY   = (1 << COUNT_BITS) - 1;
        System.out.println("CAPACITY:"+String.format("%32s", Integer.toBinaryString(CAPACITY)).replace(' ', '0'));
        System.out.println("CAPACITY:"+String.format("%32s", Integer.toBinaryString(~CAPACITY)).replace(' ', '0'));

        System.out.println(Integer.toBinaryString(1));
        System.out.println(Integer.toBinaryString(1));

        System.out.println(String.format("%32s", Integer.toBinaryString(COUNT_BITS)).replace(' ', '0'));
        System.out.println(String.format("%32s", Integer.toBinaryString(1)).replace(' ', '0'));
        System.out.println(String.format("%32s", Integer.toBinaryString((1 << COUNT_BITS))).replace(' ', '0'));

        System.out.println(String.format("%32s", Integer.toBinaryString(RUNNING)).replace(' ', '0'));
        System.out.println("SHUTDOWN:"+String.format("%32s", Integer.toBinaryString(SHUTDOWN)).replace(' ', '0'));
        System.out.println(String.format("%32s", Integer.toBinaryString(STOP)).replace(' ', '0'));
        System.out.println(String.format("%32s", Integer.toBinaryString(TIDYING)).replace(' ', '0'));
        System.out.println(String.format("%32s", Integer.toBinaryString(TERMINATED)).replace(' ', '0'));


    }
}
