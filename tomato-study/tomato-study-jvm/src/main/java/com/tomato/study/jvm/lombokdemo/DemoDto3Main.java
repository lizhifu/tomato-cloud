package com.tomato.study.jvm.lombokdemo;

import lombok.experimental.Delegate;

import java.util.ArrayList;
import java.util.List;

/**
 * DemoDto3Main
 *
 * @author lizhifu
 * @date 2022/4/27
 */
public class DemoDto3Main {
    @Delegate
    private List<String> list = new ArrayList<>(10);
    public static void main(String[] args) {
        System.out.println("hello world");
    }
}
