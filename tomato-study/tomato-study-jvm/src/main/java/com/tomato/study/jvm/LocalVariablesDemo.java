package com.tomato.study.jvm;

import java.util.Date;

/**
 * 局部变量表
 *
 * @author lizhifu
 * @date 2022/3/29
 */
public class LocalVariablesDemo {
    public static void main(String[] args) {
        Date date = new Date();
        long number = 200L;
        double salary = 6000.0;
        int count = 1;
    }
}
