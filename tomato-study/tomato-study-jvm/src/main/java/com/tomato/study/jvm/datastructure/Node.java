package com.tomato.study.jvm.datastructure;

/**
 * 节点数据
 *
 * @author lizhifu
 * @date 2022/4/25
 */
public class Node {
    /**
     * 结点中存放的数据
     */
    private int data;
    /**
     * 父结点的索引
     */
    private int parent;

    /**
     * 构造函数
     * @param data
     * @param parent
     */
    public Node(int data, int parent) {
        this.setData(data);
        this.setParent(parent);
    }

    /**
     * 重写 equals 节点数据的比较
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Node node = (Node) o;
        return data == node.data && parent == node.parent;
    }
    public int getData() {
        return data;
    }
    public void setData(int data) {
        this.data = data;
    }
    public int getParent() {
        return parent;
    }
    public void setParent(int parent) {
        this.parent = parent;
    }
}
