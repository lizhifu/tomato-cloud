package com.tomato.study.jvm.array;

/**
 * 数组
 *
 * @author lizhifu
 * @date 2022/4/13
 */
public class ArrayDemo {
    public static void main(String[] args) {
        // 静态初始化
        String[] names = new String[]{"张三","李四","王五","赵六"};

        // 动态初始化：指定数组长度
        String[] strArr = new String[5];

        // 拆分初始化
        int[] nums = new int[]{1, 2, 3, 4, 5};
        int[] arr;
        arr = nums;
    }
}
