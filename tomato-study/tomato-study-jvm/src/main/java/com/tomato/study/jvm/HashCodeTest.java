package com.tomato.study.jvm;

/**
 * hashCode
 *
 * @author lizhifu
 * @date 2022/4/12
 */
public class HashCodeTest {
    public static void main(String[] args) {
        String a = new String("str");
        String b =  new String("str");
        System.out.println(a.hashCode());
        System.out.println(a.equals(b));
        System.out.println(a == b);
    }
}
