package com.tomato.study.jvm.array;

/**
 * 数组
 *
 * @author lizhifu
 * @date 2022/4/13
 */
public class ArrayDemo2 {
    public static void main(String[] args) {
        // 静态初始化
        String[] names = new String[]{"a","b","c","d","e","f"};
        System.out.println(names[1]);
    }
}
