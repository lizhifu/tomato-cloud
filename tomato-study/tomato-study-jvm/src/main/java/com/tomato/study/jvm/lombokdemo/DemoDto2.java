package com.tomato.study.jvm.lombokdemo;


import lombok.*;
import lombok.experimental.Accessors;

/**
 * lombok demo
 *
 * @author lizhifu
 * @date 2022/4/27
 */
@Builder
public class DemoDto2 {
    private String name;
    private int age;
}