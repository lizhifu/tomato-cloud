package com.tomato.study.jvm.demo;

/**
 * 学校
 *
 * @author lizhifu
 * @date 2022/3/24
 */
public class School {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "School{" +
                "name='" + name + '\'' +
                '}';
    }
}
