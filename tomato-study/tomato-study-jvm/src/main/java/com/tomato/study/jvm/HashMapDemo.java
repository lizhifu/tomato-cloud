package com.tomato.study.jvm;

import java.util.HashMap;

/**
 * HashMapDemo
 *
 * @author lizhifu
 * @date 2022/3/30
 */
public class HashMapDemo {
    public static final int MAXIMUM_CAPACITY = 1 << 30;
    public static void main(String[] args) {
        int  i = 11;
        // 11111111111111111111111111110010   -14

        // 00000000000000000000000000001011    11
        // 00000000000000000000000000001100    12
        // 00000000000000000000000000001101    13
        // 00000000000000000000000000001110    14
        // 00000000000000000000000000010000    16 2的4次幂
        // 00000000000000000000000000100000    32 2的5次幂
        // 00000000000000000000000001000000    64 2的6次幂
        System.out.println(String.format("%32s", Integer.toBinaryString(i)).replace(' ', '0'));
        System.out.println(Integer.toBinaryString(15));

        HashMap map1 = new HashMap();
        HashMap map2 = new HashMap(16);
        HashMap map3 = new HashMap(16,0.75f);
        System.out.println(map3.toString());
        System.out.println(tableSizeFor(MAXIMUM_CAPACITY + 1));
    }
    public static int tableSizeFor(int cap) {
        System.out.println(Integer.toBinaryString(cap));
        int n = cap - 1;
        System.out.println(Integer.toBinaryString(n));
        n |= n >>> 1;
        System.out.println(Integer.toBinaryString(n));
        n |= n >>> 2;
        System.out.println(Integer.toBinaryString(n));
        n |= n >>> 4;
        System.out.println(Integer.toBinaryString(n));
        n |= n >>> 8;
        System.out.println(Integer.toBinaryString(n));
        n |= n >>> 16;
        System.out.println(Integer.toBinaryString(n));
        return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
    }
}
