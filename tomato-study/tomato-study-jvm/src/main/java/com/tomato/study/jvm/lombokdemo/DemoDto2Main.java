package com.tomato.study.jvm.lombokdemo;

/**
 * DemoDto2
 *
 * @author lizhifu
 * @date 2022/4/27
 */
public class DemoDto2Main {
    public static void main(String[] args) {
        DemoDto2 demoDto2 = DemoDto2.builder().name("tomato").age(18).build();
    }
}
