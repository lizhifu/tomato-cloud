package com.tomato.study.jvm.demo;

/**
 * Demo
 *
 * @author lizhifu
 * @date 2022/3/24
 */
public class Demo {
    public static void main(String[] args) {
        School school = new School();
        school.setName("五道口职业大学");

        Student student = new Student();
        student.setName("小宁");
        student.setSchool(school);

        System.out.println(student);
    }
}
