package com.tomato.study.jvm.array;

/**
 * 学生
 *
 * @author lizhifu
 * @date 2022/4/13
 */
public class StudentDemo {
    public static void main(String[] args) {
        int a = 12;
        Student[] students = new Student[2];
        Student student1 = new Student("张三", 20);
        Student student2 = new Student("李四", 21);
        students[0] = student1;
        students[1] = student2;
    }
}
