package com.tomato.study.jvm;

/**
 * SpringTest
 *
 * @author lizhifu
 * @date 2022/3/8
 */
public class SpringTest {
    public static void main(String[] args) {
        String a = "abc";
        String b = "abc";
        System.out.println(a == b);
        String aa = new String("abc");
        String bb = new String("abc");
        System.out.println(aa == bb);
    }
}
