package com.tomato.study.jvm.lombokdemo;

/**
 * DemoDtoMain
 *
 * @author lizhifu
 * @date 2022/4/27
 */
public class DemoDtoMain {
    public static void main(String[] args) {
        DemoDto demoDto = DemoDto.ofName("123");
        System.out.println(demoDto);
    }
}
