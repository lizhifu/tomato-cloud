package com.tomato.study.jvm;

/**
 * ParamTest
 *
 * @author lizhifu
 * @date 2022/3/16
 */
public class ParamTest2 {
    public static void main(String[] args) {
        // i 为 int 为基本数据类型，值 10 直接保存在变量中（栈）
        int i = 10;
        // 复制 i 的值 10 给形式参数 i1
        change(i);
        System.out.println("出了方法i："+i);
    }
    public static void change(int i1){
        // 对 i1 进行赋值
        i1 = 20;
        System.out.println("进入方法i："+i1);
    }
}
