package com.tomato.study.jvm;

/**
 * ParamTest
 *
 * @author lizhifu
 * @date 2022/3/16
 */
public class ParamTest3 {
    public static void main(String[] args) {
        String str = "ab";
        // 复制 i 的值 10 给形式参数 i1
        change(str);
        System.out.println("出了方法str："+str);
    }
    public static void change(String str1){
        // 对 i1 进行赋值
        str1 = "cd";
        System.out.println("进入方法str："+str1);
    }
}
