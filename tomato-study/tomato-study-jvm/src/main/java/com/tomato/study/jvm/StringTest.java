package com.tomato.study.jvm;

import org.junit.jupiter.api.Test;

/**
 * StringTest
 *
 * @author lizhifu
 * @date 2022/3/15
 */
public class StringTest {
    @Test
    public void test(){
        String a = "ab";
        change(a);
        System.out.println(a);
    }
    public static void change(String a) {
        a = "cd";
    }
    @Test
    public void test2(){
        StringBuilder a = new StringBuilder("ab");
        change2(a);
        System.out.println(a);
    }
    public static void change2(StringBuilder a) {
       a.append("cd");
    }
}
