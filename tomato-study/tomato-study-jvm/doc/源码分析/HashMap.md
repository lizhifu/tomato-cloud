# 基础

取模运算：求两个数相除的余数。

异或运算(^)：如果a、b两个值不相同，则异或结果为1。如果a、b两个值相同，异或结果为0。

位与运算(&)：只有a、b两个值的二进制同时为1，结果才为1，否则为0。

# HashMap

## 扰动函数

在HashMap存放元素时候有这样一段代码来处理哈希值，这是`java 8`的散列值扰动函数，用于优化散列效果；

```java
 public V put(K key, V value) {
        return putVal(hash(key), key, value, false, true);
    }
```

```java
static final int hash(Object key) {
        int h;
  			// h >>> 16 哈希值右移16位，正好是长度的一半
  			// ^异或运算 混合了原哈希值中的高位和低位，增大了随机性
  			// 之所以右移也是为了进行数据的混合，增大随机性
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }
```

为什么不直接只用 hashCode:

- 首先，hashCode 的取值范围是[-2^32, 2^31]，也就是[[-2147483648, 2147483647]，有将近40亿的长度，不可能把数组初始化得这么大，内存也放不下。

```java
 final V putVal(int hash, K key, V value, boolean onlyIfAbsent,
                   boolean evict) {
   									...  
										n = tab.length
   									...
                    // n 为数组长度，所以进行 -1，数组下标是从 0 开始的
                     (n - 1) & hash
                   ...
                   }
```

默认初始化的Map大小是16个长度 `DEFAULT_INITIAL_CAPACITY = 1 << 4`，所以获取的 Hash 值并不能直接作为下标使用，需要与数组长度进行取模运算（位与运算）得到一个下标值。

```
static final int DEFAULT_INITIAL_CAPACITY = 1 << 4; // aka 16
```

