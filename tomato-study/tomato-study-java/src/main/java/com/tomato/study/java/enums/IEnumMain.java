package com.tomato.study.java.enums;

/**
 * IEnum
 *
 * @author lizhifu
 * @date 2022/5/9
 */
public class IEnumMain {
    public static void main(String[] args) {
        System.out.println(DemoTypeEnum.getEnum(1).getDesc());
    }
}
