package com.tomato.study.java.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * 枚举类型
 *
 * @author lizhifu
 * @date 2022/5/9
 */
@AllArgsConstructor
@Getter
public enum DemoTypeEnum implements IEnum{
    TYPE1(1,"测试1"),
    TYPE2(2,"测试2")
    ;
    private final int code;
    private final String desc;
    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    /**
     * 根据code获取枚举类型
     * @param code
     * @return
     */
    public static DemoTypeEnum getEnum(Integer code) {
        if (Objects.isNull(code)) {
            return null;
        }
        for (DemoTypeEnum s : DemoTypeEnum.values()) {
            if (s.getCode() == code) {
                return s;
            }
        }
        return null;
    }
}
