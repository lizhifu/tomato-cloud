package com.tomato.study.java.enums;

/**
 * 枚举接口
 *
 * @author lizhifu
 * @date 2022/5/9
 */
public interface IEnum {
    /**
     * 获取枚举 code
     * @return
     */
    int getCode();

    /**
     * 获取枚举 desc
     * @return
     */
    String getDesc();
}
