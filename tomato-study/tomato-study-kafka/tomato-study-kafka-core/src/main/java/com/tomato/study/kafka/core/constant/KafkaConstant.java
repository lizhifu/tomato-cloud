package com.tomato.study.kafka.core.constant;

/**
 * KafkaConstant
 *
 * @author lizhifu
 * @date 2022/3/21
 */
public class KafkaConstant {
    /**
     * test
     */
    public static final String TOPIC_TEST = "Hello-Kafka";
    /**
     * test
     */
    public static final String TOPIC_GROUP = "test-consumer-group";
}
