package com.tomato.study.kafka.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Kafka 消费者
 *
 * @author lizhifu
 * @date 2022/3/21
 */
@SpringBootApplication
public class TomatoStudyKafkaConsumerApplication {
    public static void main(String[] args){
        SpringApplication.run(TomatoStudyKafkaConsumerApplication.class, args);
        System.out.println("Kafka 消费者 服务启动成功");
    }
}
