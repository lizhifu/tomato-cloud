package com.tomato.study.kafka.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Kafka 生产者
 *
 * @author lizhifu
 * @date 2022/3/21
 */
@SpringBootApplication
public class TomatoStudyKafkaProviderApplication {
    public static void main(String[] args){
        SpringApplication.run(TomatoStudyKafkaProviderApplication.class, args);
        System.out.println("Kafka 生产者 服务启动成功");
    }
}
