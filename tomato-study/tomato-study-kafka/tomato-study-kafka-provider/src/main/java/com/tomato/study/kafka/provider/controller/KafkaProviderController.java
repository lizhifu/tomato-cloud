package com.tomato.study.kafka.provider.controller;

import com.tomato.study.kafka.provider.provider.KafkaProvider;
import org.apache.kafka.clients.admin.TopicDescription;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * KafkaProviderController
 *
 * @author lizhifu
 * @date 2022/3/22
 */
@RestController
public class KafkaProviderController {
    @Resource
    private KafkaProvider kafkaProvider;
    @Resource
    private KafkaAdmin kafkaAdmin;
    @GetMapping("/send/{msg}")
    public String send(@PathVariable("msg") String msg){
        kafkaProvider.send(msg);
        return msg;
    }
    @GetMapping("/topic/{topic}")
    public Map queryTopic(@PathVariable("topic") String topic){
        Map<String, TopicDescription> result = kafkaAdmin.describeTopics(topic);
        return result;
    }
}
