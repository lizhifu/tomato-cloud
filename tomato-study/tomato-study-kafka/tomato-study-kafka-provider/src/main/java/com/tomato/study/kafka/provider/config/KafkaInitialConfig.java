package com.tomato.study.kafka.provider.config;

import com.tomato.study.kafka.core.constant.KafkaConstant;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * kafka 基础配置
 * 1 消费过慢的情况下，调整 Topic 分区数量
 * 2 消费者数=分区数
 * @author lizhifu
 * @date 2022/3/23
 */
@Configuration
public class KafkaInitialConfig {
    /**
     * 创建 Topic 并设置分区数为1，分区副本数为2
     * @return
     */
    @Bean
    public NewTopic initialTopic() {
        return new NewTopic(KafkaConstant.TOPIC_TEST,1, (short) 2 );
    }

    /**
     * 如果要修改分区数，只需修改配置值重启项目即可
     * 修改分区数并不会导致数据的丢失，但是分区数只能增大不能减小
     * @return
     */
    @Bean
    public NewTopic updateTopic() {
        return new NewTopic(KafkaConstant.TOPIC_TEST,2, (short) 2 );
    }
}
