package com.tomato.study.kafka.provider;

import com.tomato.study.kafka.provider.provider.KafkaProvider;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * KafkaProviderTest
 *
 * @author lizhifu
 * @date 2022/3/22
 */
@SpringBootTest
public class KafkaProviderTest {
    @Resource
    private KafkaProvider kafkaProvider;

    @Test
    public void test(){
        kafkaProvider.send("hello world !");
    }
}
