package com.tomato.study.jwt.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * jwt配置
 *
 * @author lizhifu
 * @date 2022/4/27
 */
@Component
public class AuthConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(org.springframework.web.servlet.config.annotation.InterceptorRegistry registry) {
        registry.addInterceptor(new AuthInterceptor())
                // 拦截所有请求
                .addPathPatterns("/**")
                // 排除路径
                .excludePathPatterns("/user/login");
    }
}
