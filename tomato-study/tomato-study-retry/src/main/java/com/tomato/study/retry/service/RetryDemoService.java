package com.tomato.study.retry.service;

/**
 * 重试
 *
 * @author lizhifu
 * @date 2022/5/5
 */
public interface RetryDemoService {
    String retryDemo(boolean isRetry);
}
