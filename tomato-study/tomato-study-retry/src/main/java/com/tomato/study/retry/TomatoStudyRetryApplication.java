package com.tomato.study.retry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

/**
 * 启动
 *
 * @author lizhifu
 * @date 2022/3/21
 */
@SpringBootApplication
@EnableRetry // 开启重试功能
public class TomatoStudyRetryApplication {
    public static void main(String[] args){
        SpringApplication.run(TomatoStudyRetryApplication.class, args);
        System.out.println("开启重试功能 服务启动成功");
    }
}
