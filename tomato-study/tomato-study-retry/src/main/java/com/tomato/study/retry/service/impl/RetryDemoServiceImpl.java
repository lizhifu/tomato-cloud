package com.tomato.study.retry.service.impl;

import com.tomato.study.retry.service.RetryDemoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

/**
 * retryDemoServiceImpl
 *
 * @author lizhifu
 * @date 2022/5/5
 */
@Slf4j
@Service
public class RetryDemoServiceImpl implements RetryDemoService {

    @Override
    @Retryable(value = Exception.class,maxAttempts = 3,backoff = @Backoff(delay = 2000,multiplier = 1.5))
    public String retryDemo(boolean isRetry) {
        log.info("通知下游系统");
        if (isRetry) {
            throw new RuntimeException("通知下游系统异常");
        }
        return "retryDemo";
    }

    @Recover
    public String recover(Exception e, boolean isRetry){
        log.info("执行回调方法 {},e {}",isRetry,e.getMessage());
        return "retryDemo2";
    }
}
