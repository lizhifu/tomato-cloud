package com.tomato.study.retry;

import com.tomato.study.retry.service.RetryDemoService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * RetryDemoServiceTest
 *
 * @author lizhifu
 * @date 2022/5/5
 */
@SpringBootTest
public class RetryDemoTest {
    @Resource
    private RetryDemoService retryDemoService;
    @Test
    public void test() {
        String s = retryDemoService.retryDemo(true);
        System.out.println(s);
    }
}
